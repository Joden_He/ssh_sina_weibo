package org.scau.weibo.util;

public class NameClassAndMethod {
	public static void name(){
		//new Exception().getStackTrace()[0].getClassName();当前运行的
		//new Exception().getStackTrace()[1].getClassName();当前调用的
		String classname = new Exception().getStackTrace()[1].getClassName();
		String _thisMethodName = new Exception().getStackTrace()[1].getMethodName(); 
		System.out.println("========================================================");
		System.out.println("正在运行的类名："+classname);
		System.out.println("方法名："+_thisMethodName);	
		System.out.println("========================================================");
	}
}
