/**
 * 
 */
package org.scau.weibo.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.scau.weibo.dao.impl.UserDAO;
import org.scau.weibo.vo.BUser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * @author jodenhe
 *
 */
public class TestSave {
	
	BUser user;
	UserDAO userDAO;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.scau.weibo.dao.impl.UserDAO#saveUser(org.scau.weibo.vo.BUser)}.
	 */
	@Test
	public void testSaveUser() {
		ApplicationContext ctx=new FileSystemXmlApplicationContext("src/org/scau/weibo/test/applicationContext.xml");
		
		user=new BUser();
		user.setUserEmail("824923282@qq.com");
		user.setUserName("Joden"+new java.util.Date());
		user.setUserPwd("testtest");		
		((UserDAO) ctx.getBean("userDAO")).saveUser(user);
		System.out.println(user);
	}

}
