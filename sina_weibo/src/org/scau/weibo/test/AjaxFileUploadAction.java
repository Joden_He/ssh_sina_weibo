package org.scau.weibo.test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

public class AjaxFileUploadAction {
	private File image;//接收的图�?
	private String imageFileName;//图片名称必须写，否则接收不成�?
	private String imageContentType;//图片类型必须写，否则接收不成�?
	private String message="成功";//json字符串，返回到上传页�?


	public String execute(){
		String realpath = ServletActionContext.getServletContext().getRealPath("/images");
		if (image != null) {
			String timeStr="ph"+System.currentTimeMillis();  
	        String suffix = getImageFileName().substring(getImageFileName().lastIndexOf("."));//获取后缀
			String name=timeStr+suffix;//上传后的文件名
	        File savefile= new File(realpath+File.separator+name);
			if (!savefile.getParentFile().exists())
				savefile.getParentFile().mkdirs();
			try {
				FileUtils.copyFile(image, savefile);
				
			} catch (IOException e) {
				message="上传失败";
				e.printStackTrace();
			}
		}
		return "success";
	}

	public File getImage() {
		return image;
	}

	public void setImage(File image) {
		this.image = image;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getImageContentType() {
		return imageContentType;
	}

	public void setImageContentType(String imageContentType) {
		this.imageContentType = imageContentType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}