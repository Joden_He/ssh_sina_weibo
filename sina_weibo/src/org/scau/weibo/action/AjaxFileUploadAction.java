package org.scau.weibo.action;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class AjaxFileUploadAction extends ActionSupport {
	private File image;//接收的图�?
	private String imageFileName;//图片名称必须写，否则接收不成�?
	private String imageContentType;//图片类型必须写，否则接收不成�?
	private String message="成功";//json字符串，返回到上传页�?
	private String result;
	
	public String execute(){
		String realpath = ServletActionContext.getServletContext().getRealPath("/images");
		Map<String, Object> map=new HashMap<String, Object>();
		if (image != null) {
			String timeStr="ph"+System.currentTimeMillis();  
	        String suffix = getImageFileName().substring(getImageFileName().lastIndexOf("."));//获取后缀
			String name=timeStr+suffix;//上传后的文件名
	        File savefile= new File(realpath+File.separator+name);
			if (!savefile.getParentFile().exists())
				savefile.getParentFile().mkdirs();
			try {
				FileUtils.copyFile(image, savefile);
				map.put("url", name);
				JSONObject json = JSONObject.fromObject(map);
				setResult(String.valueOf(JSONObject.fromObject(map)));
				System.out.println(this.result);
				
			} catch (IOException e) {
				message="上传失败";
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}

	public File getImage() {
		return image;
	}

	public void setImage(File image) {
		this.image = image;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getImageContentType() {
		return imageContentType;
	}

	public void setImageContentType(String imageContentType) {
		this.imageContentType = imageContentType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}