package org.scau.weibo.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.scau.weibo.services.IUserService;
import org.scau.weibo.util.NameClassAndMethod;
import org.scau.weibo.vo.BUser;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private BUser user;//属性user，用于接收从用户界面输入的用户信息
	private IUserService userService;//属性userService，用于帮助action完成相关的操作
	private String result;//属性result，用于异步传输数据交互

	@Override
	public String execute() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		return null;
	}
	
	//用户登录
	public String login() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		BUser u=userService.vaildateUser(user.getUserEmail(), user.getUserPwd());		
		if(u!=null){
			Map<String, Object> session=ActionContext.getContext().getSession();
			session.put("user", u);
			return SUCCESS;
		}
		return ERROR;
	}

	//用户登出
	public String logout() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		session.remove("user");
		return SUCCESS;
	}

	//用户注册
	public String register() throws Exception{
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		userService.saveUser(user);
		return SUCCESS;
	}

	//用户邮箱验证
	public String validateUserEmail() throws Exception{
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		HttpServletRequest request =  ServletActionContext.getRequest();
		HttpServletResponse response = (HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);  
		request.setCharacterEncoding("UTF-8");   
		response.setCharacterEncoding("UTF-8");   

		Map<String,Object> map = new HashMap<String, Object>();
		String userEmail =  request.getParameter("message");
		System.out.println(userEmail);
		if(userService.vaildateUserEmail(userEmail)){
			map.put("exitUserEmail",true);
		}
		else{
			map.put("exitUserEmail", false);
		}
		response.getWriter().print(JSONObject.fromObject(map)); 
		return null;
	}

	//用户名验证
	public String validateUserName() throws Exception{
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		HttpServletRequest request =  ServletActionContext.getRequest();
		HttpServletResponse response = (HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);  
		request.setCharacterEncoding("UTF-8");   
		response.setCharacterEncoding("UTF-8");   

		Map<String,Object> map = new HashMap<String, Object>();
		String userName =  request.getParameter("message");
		if(userService.vaildateUserName(userName)){
			map.put("exitUserName",true);
		}
		else{
			map.put("exitUserName", false);
		}
		response.getWriter().print(JSONObject.fromObject(map)); 
		return null;
	}

	//更新用户信息
	public String updateUser() throws Exception{
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser) session.get("user");
		user.setUserId(u.getUserId());
		user.setUserRegisterDate(u.getUserRegisterDate());
		if(getUser()!=null){
			session.put("user", user);
/*			JSONObject json = JSONObject.fromObject(session);//将map对象转换成json类型数据
			setResult(json.toString());//给result赋值，传递给页面
*/			System.out.println(this.result);
		}
		return SUCCESS;
	}

	public BUser getUser() {
		return user;
	}
	public void setUser(BUser user) {
		this.user = user;
	}
	public IUserService getUserService() {
		return userService;
	}
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
}
