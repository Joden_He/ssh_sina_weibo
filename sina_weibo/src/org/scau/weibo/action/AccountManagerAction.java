package org.scau.weibo.action;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.scau.weibo.services.IAccountManagerService;
import org.scau.weibo.services.IUserService;
import org.scau.weibo.util.NameClassAndMethod;
import org.scau.weibo.vo.BMessage;
import org.scau.weibo.vo.BUser;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AccountManagerAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private BMessage blog;
	private BUser user;
	private IAccountManagerService accountManagerService;
	private IUserService userService;
	private String result;

	@Override
	public String execute() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		return null;

	}

	//个人资料修改
	public String modifyUserInfo() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser)session.get("user");
		if(u!=null){
			u.setUserName(user.getUserName());
			u.setUserProvince(user.getUserProvince());
			u.setUserCity(user.getUserCity());
			u.setUserSex(user.getUserSex());
			u.setUserBirthday(user.getUserBirthday());
			u.setUserBlogAddress(user.getUserBlogAddress());
			u.setUserIntrodute(user.getUserIntrodute());
			this.userService.updateUser(u);
			session.put("user", u);
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//头像修改
	public String modifyUserHead() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser)session.get("user");
		if(u!=null){
			u.setUserHead(user.getUserHead());
			this.userService.updateUser(u);
			session.put("user", u);
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//查看个人资料
	public String showUserInfo() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser)session.get("user");
		if(u!=null){
			JSONObject json = JSONObject.fromObject(session);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//帐号密码修改
	public String modifyUserPwd() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser)session.get("user");
		if(u!=null){
			u.setUserPwd(user.getUserPwd());
			this.userService.updateUser(u);
			session.put("user", u);
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//安全手机修改
	public String modifyUserSafePhone() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser)session.get("user");
		if(u!=null){
			u.setUserPhone(user.getUserPhone());
			this.userService.updateUser(u);
			session.put("user", u);
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//个人资料职业信息修改
	public String modifyUserCom() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser)session.get("user");
		if(u!=null){
			u.setUserCompany(user.getUserCompany());
			this.userService.updateUser(u);
			session.put("user", u);
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//个人资料教育信息修改
	public String modifyUserEdu() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String, Object> session=ActionContext.getContext().getSession();
		BUser u=(BUser)session.get("user");
		if(u!=null){
			u.setUserSchool(user.getUserSchool());
			this.userService.updateUser(u);
			session.put("user", u);
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	public BMessage getBlog() {
		return blog;
	}

	public void setBlog(BMessage blog) {
		this.blog = blog;
	}

	public BUser getUser() {
		return user;
	}

	public void setUser(BUser user) {
		this.user = user;
	}

	public IAccountManagerService getAccountManagerService() {
		return accountManagerService;
	}

	public void setAccountManagerService(
			IAccountManagerService accountManagerService) {
		this.accountManagerService = accountManagerService;
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
