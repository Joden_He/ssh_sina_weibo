package org.scau.weibo.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.scau.weibo.services.IAttentionService;
import org.scau.weibo.util.NameClassAndMethod;
import org.scau.weibo.vo.BAttention;
import org.scau.weibo.vo.BUser;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AttentionAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private BAttention attention;
	private IAttentionService attentionService;
	private String result;

	@Override
	public String execute() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		return null;

	}

	//取消关注
	public String deleteAttention() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		Map<String, Object> map=new HashMap<String, Object>();
		BUser user=(BUser)session.get("user");
		if(user!=null){
			this.attentionService.deleteAttention(user.getUserId(), attention.getAttentionUserId());
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//添加关注
	public String addAttention() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		Map<String, Object> map=new HashMap<String, Object>();
		BUser user=(BUser)session.get("user");
		if(user!=null){
			Date date =new Date();
			this.attentionService.addAttention(user.getUserId(), attention.getAttentionUserId(),date);
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//移除粉丝
	public String deleteFans() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		Map<String, Object> map=new HashMap<String, Object>();
		BUser user=(BUser)session.get("user");
		if(user!=null){
			this.attentionService.deleteAttention(attention.getAttentionUserId(),user.getUserId());
			map.put("success", true);
			JSONObject json = JSONObject.fromObject(map);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//按学校名称找人
	public String findUserFromEdu() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		@SuppressWarnings("unchecked")
		Map<String, Object> request=(Map<String, Object>)ActionContext.getContext().get("request");
		BUser user=(BUser)session.get("user");
		String keyword=(String) request.get("keyword");
		if(user!=null&&keyword!=null){
			List<BUser> users=this.attentionService.findBySchool(keyword);
			request.put("users", users);
			JSONObject json = JSONObject.fromObject(request);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	//按公司名称找人
	public String findUserFromCom() throws Exception {
		NameClassAndMethod.name();//返回当前的类名和方法名，方便调试
		Map<String, Object> session=ActionContext.getContext().getSession();
		@SuppressWarnings("unchecked")
		Map<String, Object> request=(Map<String, Object>)ActionContext.getContext().get("request");
		BUser user=(BUser)session.get("user");
		String keyword=(String) request.get("keyword");
		if(user!=null&&keyword!=null){
			List<BUser> users=this.attentionService.findByCompany(keyword);
			request.put("users", users);
			JSONObject json = JSONObject.fromObject(request);
			setResult(json.toString());
			System.out.println(this.result);
		}
		return SUCCESS;
	}

	public BAttention getAttention() {
		return attention;
	}

	public void setAttention(BAttention attention) {
		this.attention = attention;
	}

	public IAttentionService getAttentionService() {
		return attentionService;
	}

	public void setAttentionService(IAttentionService attentionService) {
		this.attentionService = attentionService;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
