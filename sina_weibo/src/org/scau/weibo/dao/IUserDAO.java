package org.scau.weibo.dao;

import org.scau.weibo.vo.BUser;

public interface IUserDAO {

	/**
	 * 增加用户
	 * @param user 用户对象
	 */
	public void saveUser(BUser user);
	/**
	 * 验证用户名称是否存在
	 * @param username 用户名称
	 * @return true/false
	 */
	public boolean validateUserName(String username);

	/**
	 * 验证注册邮箱是否已注册
	 * @param userEmail 用户邮箱
	 * @return true/false
	 */
	public boolean validateUserEmail(String userEmail);

	/**
	 * 验证用户是否存在
	 * @param userEmail 用户邮箱
	 * @param password 用户密码
	 * @return
	 */
	public BUser validateUser(String userEmail,String password);

	/**
	 * 修改用户信息
	 * @param user 用户对象
	 */
	public void updateUser(BUser user);
}
