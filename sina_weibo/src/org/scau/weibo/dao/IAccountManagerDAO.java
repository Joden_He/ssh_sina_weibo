package org.scau.weibo.dao;

import java.util.List;

import org.scau.weibo.vo.BUser;

public interface IAccountManagerDAO {

	/**
	 * 根据用户ID获取用户资料信息
	 * @param userId 用户ID
	 * @return 用户对象
	 */
	public BUser getAccountAll(long userId);

	/**
	 * 根据用户名检查该用户名是否已存在
	 * @param userName 用户名
	 * @return true/false
	 */
	public boolean checkUserName(String userName);

	/**
	 * 根据用户ID修改个人基本信息
	 * @param userId 用户ID
	 * @param user 用户对象
	 * @return
	 */
	public boolean setAccountInfo(long userId ,BUser user);

	/**
	 * 根据用户ID修改个人教育信息
	 * @param userId 用户ID
	 * @param school 毕业学校
	 * @return
	 */
	public boolean setAccountEdu(long userId ,String school);

	/**
	 * 根据学校信息获取用户信息
	 * @param school 学校名
	 * @return
	 */
	public List<BUser> getAccountByEdu(String school);

	/**
	 * 根据用户ID修改个人职业信息
	 * @param userId 用户ID
	 * @param company 公司名称
	 * @return
	 */
	public boolean setAccountProfession(long userId ,String company);

	/**
	 * 根据用户ID修改个人头像
	 * @param userId 用户ID
	 * @param headurl 头像路径
	 * @return true/false
	 */
	public boolean setAccountHeadpicture(long userId ,String headurl);
	
	/**
	 * 根据用户ID、旧密码、新密码修改个人密码
	 * @param userId 用户ID
	 * @param oldpwd 旧密码
	 * @param newpwd 新密码
	 * @return true/false
	 */
	public boolean modifyAccountPassword(long userId ,String oldpwd,String newpwd);

	/**
	 * 根据用户ID、手机号码、新密码重置个人密码
	 * @param userId 用户ID
	 * @param phonenum 手机号码
	 * @param newpwd 新密码
	 * @return true/false
	 */
	public boolean resetAccountPassword(long userId ,String phonenum,String newpwd);
}
