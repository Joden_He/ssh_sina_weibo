package org.scau.weibo.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.scau.weibo.dao.BaseDAO;
import org.scau.weibo.dao.IBlogDAO;
import org.scau.weibo.vo.BCollect;
import org.scau.weibo.vo.BComment;
import org.scau.weibo.vo.BMessage;
import org.scau.weibo.vo.BUser;

public class BlogDAO extends BaseDAO implements IBlogDAO{

	@Override
	public List<BMessage> queryBlogAll() {
		
		Session session=getSession();
		String hql="from BMessage m order by m.messageDate desc";
		Query query=session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}


	@Override
	public List<BMessage> queryAttentionBlogAll(long userId) {
		
		Session session=getSession();
		String hql="from BMessage m,BAttention a where a.attentionUserId=m.BUser.userId and a.BUser.userId=? order by m.messageDate desc";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public List<BMessage> queryOriginalBlogAll() {
		
		Session session=getSession();
		String hql="from BMessage m where m.messageIsForward=0 order by m.messageDate desc";
		Query query=session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public List<BMessage> queryOriginalBlogAll(int start, int end) {
		
		Session session=getSession();
		String hql="from BMessage m where m.messageIsForward=0 order by m.messageDate desc";
		Query query=session.createQuery(hql);
		query.setFirstResult(start);
		query.setMaxResults(end);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public List<BMessage> queryPictureBlogAll(int start, int end) {
		
		Session session=getSession();
		String hql="from BMessage m where m.messageLink like '%/%' order by m.messageDate desc";
		Query query=session.createQuery(hql);
		query.setFirstResult(start);
		query.setMaxResults(end);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public List<BMessage> queryRelateBlogAll(String userName, int start, int end) {
		
		Session session=getSession();
		String hql="from BMessage m,BUser u where u.userName=? and m.BUser=u order by m.messageDate desc";
		Query query=session.createQuery(hql);
		query.setParameter(0, userName);
		query.setFirstResult(start);
		query.setMaxResults(end);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public List<BMessage> queryBlogAll(String userName, int page) {
		
		Session session=getSession();
		String hql="from BMessage m where m.messageContent like '%@"+userName+"%' order by m.messageDate desc";
		Query query=session.createQuery(hql);
		int start=(page-1)*10+1;
		int end=page*10;
		query.setFirstResult(start);
		query.setMaxResults(end);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public List<BMessage> queryBlogAll(int start, int end) {
		
		Session session=getSession();
		String hql="from BMessage m order by m.messageDate desc";
		Query query=session.createQuery(hql);
		query.setFirstResult(start);
		query.setMaxResults(end);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public List<BMessage> queryBlogAll(long userId) {
		
		Session session=getSession();
		String hql="from BMessage m where m.BUser.userId=? order by m.messageDate desc";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

	@Override
	public BMessage queryBlogl(long blogId) {
		
		Session session=getSession();
		String hql="from BMessage m where m.messageId=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, blogId);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		if(blogs.size()!=0){
			return blogs.get(0);
		}
		session.close();
		return null;

	}

	@Override
	public void addBlog(BMessage blog) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		session.save(blog);
		tx.commit();
		session.close();
	}

	@Override
	public void deleteBlog(long blogId) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BMessage blog=(BMessage)session.get(BMessage.class, blogId);
		session.delete(blog);
		tx.commit();
		session.close();
	}

	@Override
	public void addCollectCount(long blogId, int count) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BMessage blog=(BMessage)session.get(BMessage.class, blogId);
		blog.setMessageCollect((long) count);
		session.update(blog);
		tx.commit();
		session.close();
	}

	@Override
	public void addForwardCount(long blogId, int count) {
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BMessage blog=(BMessage)session.get(BMessage.class, blogId);
		
		String hql="from BMessage b where b.messageId="+blogId+" or b.messageForwardId="+blogId;
		Query query=session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		for (int i = 0; i < blogs.size(); i++) {
			blogs.get(i).setMessageForward((long) count);
			session.update(blog);
		}
		tx.commit();
		session.close();
	}

	@Override
	public void addReviewCount(long blogId, int count) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BMessage blog=(BMessage)session.get(BMessage.class, blogId);
		blog.setMessageReview((long) count);
		session.update(blog);
		tx.commit();
		session.close();
	}

	@Override
	public void insertCollect(long userId, long blogId, Date date) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BMessage blog=(BMessage)session.get(BMessage.class, blogId);
		BUser user=(BUser)session.get(BUser.class, userId);
		BCollect collect=new BCollect();
		collect.setBMessage(blog);
		collect.setBUser(user);
		collect.setCollectDate(date);
		session.save(blog);
		tx.commit();
		session.close();

	}

	@Override
	public void deleteCollect(long userId, long blogId) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BMessage blog=(BMessage)session.get(BMessage.class, blogId);
		BUser user=(BUser)session.get(BUser.class, userId);
		String hql="from BCollect c where c.BUser=? and c.BMessage=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, user);
		query.setParameter(1, blog);
		@SuppressWarnings("unchecked")
		List<BCollect> collects=query.list();
		for (int i = 0; i < collects.size(); i++) {
			session.delete(collects.get(i));
		}
		tx.commit();
		session.close();

	}

	@Override
	public void deleteCollect(long blogId) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BMessage blog=(BMessage)session.get(BMessage.class, blogId);
		String hql="from BCollect c where c.BMessage=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, blog);
		@SuppressWarnings("unchecked")
		List<BCollect> collects=query.list();
		for (int i = 0; i < collects.size(); i++) {
			session.delete(collects.get(i));
		}
		tx.commit();
		session.close();
	}

	@Override
	public List<BMessage> queryCollect(long userId) {
		
		Session session=getSession();
		String hql="from BCollect c where c.BUser.userId=? order by c.collectDate desc";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		@SuppressWarnings("unchecked")
		List<BCollect> collects=query.list();
		List<BMessage> blogs=new ArrayList<BMessage>();
		for (int i = 0; i < collects.size(); i++) {
			blogs.add(collects.get(i).getBMessage());
		}
		session.close();
		return blogs;
	}

	@Override
	public BMessage queryCollect(long userId, long blogId) {
		
		Session session=getSession();
		String hql="from BCollect c where c.BUser.userId=? and c.BMessage.messageId=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		query.setParameter(1, blogId);
		@SuppressWarnings("unchecked")
		List<BCollect> collects=query.list();
		if(collects.size()!=0){
			BMessage blog=collects.get(0).getBMessage();
			return blog;
		}
		session.close();
		return null;
	}

	@Override
	public List<BMessage> queryCollect(long userId, int start, int end) {
		
		Session session=getSession();
		String hql="from BCollect c where c.BUser.userId=? order by c.collectDate desc";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		query.setFirstResult(start);
		query.setMaxResults(end);
		@SuppressWarnings("unchecked")
		List<BCollect> collects=query.list();
		List<BMessage> blogs=new ArrayList<BMessage>();
		for (int i = 0; i < collects.size(); i++) {
			blogs.add(collects.get(i).getBMessage());
		}
		session.close();
		return blogs;
	}

	@Override
	public List<BComment> queryComment(long blogId) {
		
		Session session=getSession();
		String hql="from BComment c where c.BMessage.messageId=? order by c.messageDate desc";
		Query query=session.createQuery(hql);
		query.setParameter(0, blogId);
		@SuppressWarnings("unchecked")
		List<BComment> comments=query.list();
		session.close();
		return comments;
	}

	@Override
	public void insertComment(BComment comment) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		session.save(comment);
		tx.commit();
		session.close();

	}

	@Override
	public int getBlogSize() {
		return queryBlogAll().size();
	}

	@Override
	public int getBlogOriginalSize() {
		return queryOriginalBlogAll().size();
	}

	@Override
	public int getBlogPictureSize() {
		Session session=getSession();
		String hql="from BMessage m where m.messageLink like '%/%'";
		Query query=session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs.size();
	}

	@Override
	public int getBlogSize(long userId) {
		return queryBlogAll(userId).size();
	}

	@Override
	public int getCollectBlogSize(long userId) {
		return queryCollect(userId).size();
	}

	@Override
	public void deleteComment(long commentId) {
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BComment comment=(BComment)session.get(BComment.class, commentId);
		session.delete(comment);
		tx.commit();
		session.close();

	}

	@Override
	public int createPictureName() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRelateBlogSize(String userName) {
		Session session=getSession();
		String hql="from BMessage m,BUser u where u.userName=? and m.BUser=u";
		Query query=session.createQuery(hql);
		query.setParameter(0, userName);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs.size();
	}

	@Override
	public List<BMessage> getPictureBlogAll() {
		Session session=getSession();
		String hql="from BMessage m where m.messageLink like '%/%' order by m.messageDate desc";
		Query query=session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}


	@Override
	public void forward(BUser user,long blogId) {
		Session session=getSession();
		String hql="from BMessage m where m.messageId=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, blogId);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		if(blogs.size()!=0){
			BMessage b=blogs.get(0);
			Date date=new Date(); 
			b.setMessageContent("@"+b.getBUser().getUserName()+"   "+b.getMessageContent());
			b.setBUser(user);
			b.setMessageDate(date);
			b.setMessageIsForward((short) 1);
			
			Transaction tx=session.beginTransaction();
			session.save(b);
//			BMessage blog2=(BMessage)session.get(BMessage.class, blogId);
//			blog2.setMessageForward((long) b.getMessageForward()+1);
//			session.update(blog2);
			tx.commit();
			session.close();
			addForwardCount(blogId,(int) (b.getMessageForward()==null?1:b.getMessageForward()+1));
			System.out.println("ok");
		}
		
		
	}


	@Override
	public List<BMessage> search(String k) {
		Session session=getSession();
		String hql="from BMessage m where  m.messageContent like '%"+k+"%' order by m.messageDate desc";
		Query query=session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BMessage> blogs=query.list();
		session.close();
		return blogs;
	}

}