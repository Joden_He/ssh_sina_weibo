package org.scau.weibo.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.scau.weibo.dao.BaseDAO;
import org.scau.weibo.dao.IAccountManagerDAO;
import org.scau.weibo.vo.BUser;

public class AccountManagerDAO extends BaseDAO implements IAccountManagerDAO{

	@Override
	public BUser getAccountAll(long userId) {
		
		Session session=getSession();
		String hql="from BUser u where u.userId=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		List<?> users=query.list();
		if(users.size()!=0){
			BUser user=(BUser)users.get(0);
			return user;
		}
		session.close();
		return null;
	}

	@Override
	public boolean checkUserName(String userName) {
		
		Session session=getSession();
		String hql="from BUser u where u.userName=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, userName);
		List<?> users=query.list();
		if(users.size()!=0){
			return true;
		}
		session.close();
		return false;
	}

	@Override
	public boolean setAccountInfo(long userId, BUser user) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		user.setUserId((long)userId);
		session.update(user);
		tx.commit();
		session.close();	
		return true;
	}

	@Override
	public boolean setAccountEdu(long userId, String school) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BUser user=(BUser)session.get(BUser.class, userId); 
		user.setUserSchool(school);
		session.update(user);
		tx.commit();
		session.close();	
		return true;
	}

	@Override
	public List<BUser> getAccountByEdu(String school) {
		
		Session session=getSession();
		String hql="from BUser u where u.userSchool=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, school);
		@SuppressWarnings("unchecked")
		List<BUser> users=query.list();
		session.close();
		return users;
	}

	@Override
	public boolean setAccountProfession(long userId, String company) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BUser user=(BUser)session.get(BUser.class, userId); 
		user.setUserCompany(company);
		session.update(user);
		tx.commit();
		session.close();	
		return true;
	}

	@Override
	public boolean setAccountHeadpicture(long userId, String headurl) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BUser user=(BUser)session.get(BUser.class, userId); 
		user.setUserHead(headurl);
		session.update(user);
		tx.commit();
		session.close();	
		return true;
	}

	@Override
	public boolean modifyAccountPassword(long userId, String oldpwd,
			String newpwd) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BUser user=(BUser)session.get(BUser.class, userId);  
		user.setUserPwd(newpwd);
		session.update(user);
		tx.commit();
		session.close();	
		return true;
	}

	@Override
	public boolean resetAccountPassword(long userId, String phonenum,
			String newpwd) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BUser user=(BUser)session.get(BUser.class, userId); 
		user.setUserPwd(newpwd);
		session.update(user);
		tx.commit();
		session.close();	
		return true;
	}

}
