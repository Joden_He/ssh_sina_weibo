package org.scau.weibo.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.scau.weibo.dao.BaseDAO;
import org.scau.weibo.dao.IAttentionDAO;
import org.scau.weibo.vo.BAttention;
import org.scau.weibo.vo.BUser;

public class AttentionDAO extends BaseDAO implements IAttentionDAO{

	@Override
	public List<BUser> findAttention(long userId) {
		
		Session session=getSession();
		String hql="from BAttention a where a.userId=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		@SuppressWarnings("unchecked")
		List<BAttention> attentions=query.list();
		List<BUser> users=new ArrayList<BUser>();
		for (int i = 0; i < attentions.size(); i++) {
			users.add(attentions.get(i).getBUser());
		}
		return users;
	}

	@Override
	public void addAttention(long userId, long attentionUserId, Date date) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BAttention attention=new BAttention();
		BUser user=(BUser)session.get(BUser.class, userId);
		attention.setBUser(user);
		attention.setAttentionUserId(attentionUserId);
		attention.setAttentionDate(date);
		session.update(user);
		tx.commit();
		session.close();	
		
	}

	@Override
	public void deleteAttention(long userId, long attentionUserId) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		String hql="from BAttention a where a.userId=? a.attentionUserId=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, userId);
		query.setParameter(1, attentionUserId);
		@SuppressWarnings("unchecked")
		List<BAttention> attentions=query.list();
		if(attentions.size()>0){
			session.delete(attentions.get(0));
			tx.commit();
		}
		session.close();
		
	}

	@Override
	public List<BUser> findFans(long attentionUserId) {
		
		Session session=getSession();
		String hql="from BAttention a where a.attentionUserId=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, attentionUserId);
		@SuppressWarnings("unchecked")
		List<BAttention> attentions=query.list();
		List<BUser> users=new ArrayList<BUser>();
		for (int i = 0; i < attentions.size(); i++) {
			users.add(attentions.get(i).getBUser());
		}
		return users;
	}

	@Override
	public void addFans(long userId, long attentionUserId) {
		
		Session session=getSession();
		Transaction tx=session.beginTransaction();
		BAttention attention=new BAttention();
		BUser user=(BUser)session.get(BUser.class, userId);
		Date date=new Date();
		attention.setAttentionDate(date);
		attention.setAttentionUserId(attentionUserId);
		attention.setBUser(user);
		session.save(attention);
		tx.commit();
		session.close();
	}

	@Override
	public List<BUser> findByName(String userName) {
		
		Session session=getSession();
		String hql="from BUser u where u.userName=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, userName);
		@SuppressWarnings("unchecked")
		List<BUser> users=query.list();
		return users;
	}

	@Override
	public List<BUser> findBySchool(String school) {
		
		Session session=getSession();
		String hql="from BUser u where u.userSchool=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, school);
		@SuppressWarnings("unchecked")
		List<BUser> users=query.list();
		return users;
	}

	@Override
	public List<BUser> findByCompany(String company) {
		
		Session session=getSession();
		String hql="from BUser u where u.userCompany=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, company);
		@SuppressWarnings("unchecked")
		List<BUser> users=query.list();
		return users;
	}

}
