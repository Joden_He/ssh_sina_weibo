package org.scau.weibo.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BCollect entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "b_collect", catalog = "db_sina")
public class BCollect implements java.io.Serializable {

	// Fields

	private Long collectId;
	private BUser BUser;
	private BMessage BMessage;
	private Date collectDate;
	private String collectRemark;

	// Constructors

	/** default constructor */
	public BCollect() {
	}

	/** minimal constructor */
	public BCollect(BUser BUser, BMessage BMessage) {
		this.BUser = BUser;
		this.BMessage = BMessage;
	}

	/** full constructor */
	public BCollect(BUser BUser, BMessage BMessage, Date collectDate,
			String collectRemark) {
		this.BUser = BUser;
		this.BMessage = BMessage;
		this.collectDate = collectDate;
		this.collectRemark = collectRemark;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "collectId", unique = true, nullable = false)
	public Long getCollectId() {
		return this.collectId;
	}

	public void setCollectId(Long collectId) {
		this.collectId = collectId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId", nullable = false)
	public BUser getBUser() {
		return this.BUser;
	}

	public void setBUser(BUser BUser) {
		this.BUser = BUser;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "messageId", nullable = false)
	public BMessage getBMessage() {
		return this.BMessage;
	}

	public void setBMessage(BMessage BMessage) {
		this.BMessage = BMessage;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "collectDate", length = 10)
	public Date getCollectDate() {
		return this.collectDate;
	}

	public void setCollectDate(Date collectDate) {
		this.collectDate = collectDate;
	}

	@Column(name = "collectRemark", length = 100)
	public String getCollectRemark() {
		return this.collectRemark;
	}

	public void setCollectRemark(String collectRemark) {
		this.collectRemark = collectRemark;
	}
	@Override
	public String toString() {
		return "BCollect [collectId=" + collectId + ", BUser=" + BUser
				+ ", BMessage=" + BMessage + ", collectDate=" + collectDate
				+ ", collectRemark=" + collectRemark + "]";
	}
	
	public Map<String,Object> toMap(){
		Map<String,Object> map=new HashMap<String, Object>();
		String date = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss").format(collectDate);

		map.put("userId", BUser.getUserId());
		map.put("collectId", collectId);
		map.put("messageId", BMessage.getMessageId());
		map.put("collectRemark", collectRemark);
		map.put("collectDate", date);
		return map;
	}

	
}