package org.scau.weibo.vo;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * BUser entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "b_user", catalog = "db_sina")
public class BUser implements java.io.Serializable {

	// Fields

	private Long userId;
	private String userName;
	private String userHead;
	private String userEmail;
	private String userPwd;
	private String userSex;
	private String userSchool;
	private String userCompany;
	private String userPhone;
	private String userProvince;
	private String userCity;
	private String userBlogAddress;
	private Timestamp userRegisterDate;
	private Timestamp userBirthday;
	private String usrBackground;
	private String userIntrodute;
	private String userRemark;
	private Set<BMessage> BMessages = new HashSet<BMessage>(0);
	private Set<BAttention> BAttentions = new HashSet<BAttention>(0);
	private Set<BCollect> BCollects = new HashSet<BCollect>(0);
	private Set<BComment> BComments = new HashSet<BComment>(0);

	// Constructors

	/** default constructor */
	public BUser() {
	}

	/** minimal constructor */
	public BUser(String userName, String userEmail, Timestamp userRegisterDate) {
		this.userName = userName;
		this.userEmail = userEmail;
		this.userRegisterDate = userRegisterDate;
	}

	/** full constructor */
	public BUser(String userName, String userHead, String userEmail,
			String userPwd, String userSex, String userSchool,
			String userCompany, String userPhone, String userProvince,
			String userCity, String userBlogAddress,
			Timestamp userRegisterDate, Timestamp userBirthday,
			String usrBackground, String userIntrodute, String userRemark,
			Set<BMessage> BMessages, Set<BAttention> BAttentions,
			Set<BCollect> BCollects, Set<BComment> BComments) {
		this.userName = userName;
		this.userHead = userHead;
		this.userEmail = userEmail;
		this.userPwd = userPwd;
		this.userSex = userSex;
		this.userSchool = userSchool;
		this.userCompany = userCompany;
		this.userPhone = userPhone;
		this.userProvince = userProvince;
		this.userCity = userCity;
		this.userBlogAddress = userBlogAddress;
		this.userRegisterDate = userRegisterDate;
		this.userBirthday = userBirthday;
		this.usrBackground = usrBackground;
		this.userIntrodute = userIntrodute;
		this.userRemark = userRemark;
		this.BMessages = BMessages;
		this.BAttentions = BAttentions;
		this.BCollects = BCollects;
		this.BComments = BComments;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "userId", unique = true, nullable = false)
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "userName", nullable = false, length = 20)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "userHead", length = 100)
	public String getUserHead() {
		return this.userHead;
	}

	public void setUserHead(String userHead) {
		this.userHead = userHead;
	}

	@Column(name = "userEmail", nullable = false, length = 50)
	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@Column(name = "userPwd", length = 20)
	public String getUserPwd() {
		return this.userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	@Column(name = "userSex", length = 10)
	public String getUserSex() {
		return this.userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	@Column(name = "userSchool", length = 50)
	public String getUserSchool() {
		return this.userSchool;
	}

	public void setUserSchool(String userSchool) {
		this.userSchool = userSchool;
	}

	@Column(name = "userCompany", length = 50)
	public String getUserCompany() {
		return this.userCompany;
	}

	public void setUserCompany(String userCompany) {
		this.userCompany = userCompany;
	}

	@Column(name = "userPhone", length = 40)
	public String getUserPhone() {
		return this.userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	@Column(name = "userProvince", length = 20)
	public String getUserProvince() {
		return this.userProvince;
	}

	public void setUserProvince(String userProvince) {
		this.userProvince = userProvince;
	}

	@Column(name = "userCity", length = 20)
	public String getUserCity() {
		return this.userCity;
	}

	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}

	@Column(name = "userBlogAddress", length = 100)
	public String getUserBlogAddress() {
		return this.userBlogAddress;
	}

	public void setUserBlogAddress(String userBlogAddress) {
		this.userBlogAddress = userBlogAddress;
	}

	@Column(name = "userRegisterDate", nullable = false, length = 19)
	public Timestamp getUserRegisterDate() {
		return this.userRegisterDate;
	}

	public void setUserRegisterDate(Timestamp userRegisterDate) {
		this.userRegisterDate = userRegisterDate;
	}

	@Column(name = "userBirthday", length = 19)
	public Timestamp getUserBirthday() {
		return this.userBirthday;
	}

	public void setUserBirthday(Timestamp userBirthday) {
		this.userBirthday = userBirthday;
	}

	@Column(name = "usrBackground", length = 100)
	public String getUsrBackground() {
		return this.usrBackground;
	}

	public void setUsrBackground(String usrBackground) {
		this.usrBackground = usrBackground;
	}

	@Column(name = "userIntrodute", length = 100)
	public String getUserIntrodute() {
		return this.userIntrodute;
	}

	public void setUserIntrodute(String userIntrodute) {
		this.userIntrodute = userIntrodute;
	}

	@Column(name = "userRemark", length = 100)
	public String getUserRemark() {
		return this.userRemark;
	}

	public void setUserRemark(String userRemark) {
		this.userRemark = userRemark;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "BUser")
	public Set<BMessage> getBMessages() {
		return this.BMessages;
	}

	public void setBMessages(Set<BMessage> BMessages) {
		this.BMessages = BMessages;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "BUser")
	public Set<BAttention> getBAttentions() {
		return this.BAttentions;
	}

	public void setBAttentions(Set<BAttention> BAttentions) {
		this.BAttentions = BAttentions;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "BUser")
	public Set<BCollect> getBCollects() {
		return this.BCollects;
	}

	public void setBCollects(Set<BCollect> BCollects) {
		this.BCollects = BCollects;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "BUser")
	public Set<BComment> getBComments() {
		return this.BComments;
	}

	public void setBComments(Set<BComment> BComments) {
		this.BComments = BComments;
	}

	@Override
	public String toString() {
		return "BUser [userId=" + userId + ", userName=" + userName
				+ ", userHead=" + userHead + ", userEmail=" + userEmail
				+ ", userPwd=" + userPwd + ", userSex=" + userSex
				+ ", userSchool=" + userSchool + ", userCompany=" + userCompany
				+ ", userPhone=" + userPhone + ", userProvince=" + userProvince
				+ ", userCity=" + userCity + ", userBlogAddress="
				+ userBlogAddress + ", userRegisterDate=" + userRegisterDate
				+ ", userBirthday=" + userBirthday + ", usrBackground="
				+ usrBackground + ", userIntrodute=" + userIntrodute
				+ ", userRemark=" + userRemark + "]";
	}

	public Map<String,Object> toMap(){
		Map<String,Object> map=new LinkedHashMap<String, Object>();
		String registerDate = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss").format(userRegisterDate);
		String birthday = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss").format(userRegisterDate);

		map.put("userId", userId);
		map.put("userName", userName);
		map.put("userHead", userHead);
		map.put("userEmail", userEmail);
		map.put("userSex", userSex);
		map.put("userSchool", userSchool);
		map.put("userCompany", userCompany);
		map.put("userPhone", userPhone);
		map.put("userProvince", userProvince);
		map.put("userCity", userCity);
		map.put("userBlogAddress", userBlogAddress);
		map.put("userRegisterDate", registerDate);
		map.put("userBirthday", birthday);
		map.put("usrBackground", usrBackground);
		map.put("userIntrodute", userIntrodute);
		map.put("userRemark", userRemark);
		return map;
	}

}