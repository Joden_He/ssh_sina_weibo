package org.scau.weibo.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BComment entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "b_comment", catalog = "db_sina")
public class BComment implements java.io.Serializable {

	// Fields

	private Long commentId;
	private BUser BUser;
	private BMessage BMessage;
	private String messageContent;
	private Date messageDate;
	private String messageRemark;
	private Long commentedUserId;

	// Constructors

	/** default constructor */
	public BComment() {
	}

	/** minimal constructor */
	public BComment(BMessage BMessage) {
		this.BMessage = BMessage;
	}

	/** full constructor */
	public BComment(BUser BUser, BMessage BMessage, String messageContent,
			Date messageDate, String messageRemark, Long commentedUserId) {
		this.BUser = BUser;
		this.BMessage = BMessage;
		this.messageContent = messageContent;
		this.messageDate = messageDate;
		this.messageRemark = messageRemark;
		this.commentedUserId = commentedUserId;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "commentId", unique = true, nullable = false)
	public Long getCommentId() {
		return this.commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	public BUser getBUser() {
		return this.BUser;
	}

	public void setBUser(BUser BUser) {
		this.BUser = BUser;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "messageId", nullable = false)
	public BMessage getBMessage() {
		return this.BMessage;
	}

	public void setBMessage(BMessage BMessage) {
		this.BMessage = BMessage;
	}

	@Column(name = "messageContent", length = 300)
	public String getMessageContent() {
		return this.messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "messageDate", length = 10)
	public Date getMessageDate() {
		return this.messageDate;
	}

	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	@Column(name = "messageRemark", length = 100)
	public String getMessageRemark() {
		return this.messageRemark;
	}

	public void setMessageRemark(String messageRemark) {
		this.messageRemark = messageRemark;
	}

	@Column(name = "commentedUserId")
	public Long getCommentedUserId() {
		return this.commentedUserId;
	}

	public void setCommentedUserId(Long commentedUserId) {
		this.commentedUserId = commentedUserId;
	}
	@Override
	public String toString() {
		return "BComment [commentId=" + commentId + ", BUser=" + BUser
				+ ", BMessage=" + BMessage + ", messageContent="
				+ messageContent + ", messageDate=" + messageDate
				+ ", messageRemark=" + messageRemark + ", commentedUserId="
				+ commentedUserId + "]";
	}

	public Map<String,Object> toMap(){
		Map<String,Object> map=new HashMap<String, Object>();
		String date = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss").format(messageDate);

		map.put("userId", BUser.getUserId());
		map.put("commentId", commentId);
		map.put("messageId", BMessage.getMessageId());
		map.put("messageContent", messageContent);
		map.put("messageDate", date);
		map.put("messageRemark", messageRemark);
		map.put("commentedUserId", commentedUserId);
		return map;
	}

}