package org.scau.weibo.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BAttention entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "b_attention", catalog = "db_sina")
public class BAttention implements java.io.Serializable {

	// Fields

	private Long attentionId;
	private BUser BUser;
	private Long attentionUserId;
	private Date attentionDate;
	private String attentionRemark;

	// Constructors

	/** default constructor */
	public BAttention() {
	}

	/** minimal constructor */
	public BAttention(BUser BUser) {
		this.BUser = BUser;
	}

	/** full constructor */
	public BAttention(BUser BUser, Long attentionUserId, Date attentionDate,
			String attentionRemark) {
		this.BUser = BUser;
		this.attentionUserId = attentionUserId;
		this.attentionDate = attentionDate;
		this.attentionRemark = attentionRemark;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "attentionId", unique = true, nullable = false)
	public Long getAttentionId() {
		return this.attentionId;
	}

	public void setAttentionId(Long attentionId) {
		this.attentionId = attentionId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId", nullable = false)
	public BUser getBUser() {
		return this.BUser;
	}

	public void setBUser(BUser BUser) {
		this.BUser = BUser;
	}

	@Column(name = "attentionUserId")
	public Long getAttentionUserId() {
		return this.attentionUserId;
	}

	public void setAttentionUserId(Long attentionUserId) {
		this.attentionUserId = attentionUserId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "attentionDate", length = 10)
	public Date getAttentionDate() {
		return this.attentionDate;
	}

	public void setAttentionDate(Date attentionDate) {
		this.attentionDate = attentionDate;
	}

	@Column(name = "attentionRemark", length = 100)
	public String getAttentionRemark() {
		return this.attentionRemark;
	}

	public void setAttentionRemark(String attentionRemark) {
		this.attentionRemark = attentionRemark;
	}
	
	@Override
	public String toString() {
		return "BAttention [attentionId=" + attentionId + ", BUser=" + BUser
				+ ", attentionUserId=" + attentionUserId + ", attentionDate="
				+ attentionDate + ", attentionRemark=" + attentionRemark + "]";
	}

	public Map<String,Object> toMap(){
		Map<String,Object> map=new HashMap<String, Object>();
		String date = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss").format(attentionDate);

		map.put("userId", BUser.getUserId());
		map.put("attentionId", attentionId);
		map.put("attentionUserId", attentionUserId);
		map.put("attentionRemark", attentionRemark);
		map.put("attentionDate", date);
		return map;
	}

}