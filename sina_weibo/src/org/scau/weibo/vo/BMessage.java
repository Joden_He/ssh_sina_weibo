package org.scau.weibo.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BMessage entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "b_message", catalog = "db_sina")
public class BMessage implements java.io.Serializable {

	// Fields

	private Long messageId;
	private BUser BUser;
	private String messageContent;
	private Long messageCollect;
	private Long messageForward;
	private Long messageReview;
	private Date messageDate;
	private Short messageIsForward;
	private Long messageForwardId;
	private String messageLink;
	private String messageRemark;
	private Set<BComment> BComments = new HashSet<BComment>(0);
	private Set<BCollect> BCollects = new HashSet<BCollect>(0);

	// Constructors

	/** default constructor */
	public BMessage() {
	}

	/** minimal constructor */
	public BMessage(BUser BUser) {
		this.BUser = BUser;
	}

	/** full constructor */
	public BMessage(BUser BUser, String messageContent, Long messageCollect,
			Long messageForward, Long messageReview, Date messageDate,
			Short messageIsForward, Long messageForwardId, String messageLink,
			String messageRemark, Set<BComment> BComments,
			Set<BCollect> BCollects) {
		this.BUser = BUser;
		this.messageContent = messageContent;
		this.messageCollect = messageCollect;
		this.messageForward = messageForward;
		this.messageReview = messageReview;
		this.messageDate = messageDate;
		this.messageIsForward = messageIsForward;
		this.messageForwardId = messageForwardId;
		this.messageLink = messageLink;
		this.messageRemark = messageRemark;
		this.BComments = BComments;
		this.BCollects = BCollects;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "messageId", unique = true, nullable = false)
	public Long getMessageId() {
		return this.messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId", nullable = false)
	public BUser getBUser() {
		return this.BUser;
	}

	public void setBUser(BUser BUser) {
		this.BUser = BUser;
	}

	@Column(name = "messageContent", length = 300)
	public String getMessageContent() {
		return this.messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	@Column(name = "messageCollect")
	public Long getMessageCollect() {
		return this.messageCollect;
	}

	public void setMessageCollect(Long messageCollect) {
		this.messageCollect = messageCollect;
	}

	@Column(name = "messageForward")
	public Long getMessageForward() {
		return this.messageForward;
	}

	public void setMessageForward(Long messageForward) {
		this.messageForward = messageForward;
	}

	@Column(name = "messageReview")
	public Long getMessageReview() {
		return this.messageReview;
	}

	public void setMessageReview(Long messageReview) {
		this.messageReview = messageReview;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "messageDate", length = 10)
	public Date getMessageDate() {
		return this.messageDate;
	}

	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	@Column(name = "messageIsForward")
	public Short getMessageIsForward() {
		return this.messageIsForward;
	}

	public void setMessageIsForward(Short messageIsForward) {
		this.messageIsForward = messageIsForward;
	}

	@Column(name = "messageForwardId")
	public Long getMessageForwardId() {
		return this.messageForwardId;
	}

	public void setMessageForwardId(Long messageForwardId) {
		this.messageForwardId = messageForwardId;
	}

	@Column(name = "messageLink", length = 100)
	public String getMessageLink() {
		return this.messageLink;
	}

	public void setMessageLink(String messageLink) {
		this.messageLink = messageLink;
	}

	@Column(name = "messageRemark", length = 100)
	public String getMessageRemark() {
		return this.messageRemark;
	}

	public void setMessageRemark(String messageRemark) {
		this.messageRemark = messageRemark;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "BMessage")
	public Set<BComment> getBComments() {
		return this.BComments;
	}

	public void setBComments(Set<BComment> BComments) {
		this.BComments = BComments;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "BMessage")
	public Set<BCollect> getBCollects() {
		return this.BCollects;
	}

	public void setBCollects(Set<BCollect> BCollects) {
		this.BCollects = BCollects;
	}
	@Override
	public String toString() {
		return "BMessage [messageId=" + messageId + ", BUser=" + BUser
				+ ", messageContent=" + messageContent + ", messageCollect="
				+ messageCollect + ", messageForward=" + messageForward
				+ ", messageReview=" + messageReview + ", messageDate="
				+ messageDate + ", messageIsForward=" + messageIsForward
				+ ", messageForwardId=" + messageForwardId + ", messageLink="
				+ messageLink + ", messageRemark=" + messageRemark + "]";
	}

	public Map<String,Object> toMap(){
		Map<String,Object> map=new HashMap<String, Object>();
		String date = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss").format(messageDate);
		map.put("userId", BUser.getUserId());
		map.put("userName", BUser.getUserName());
		map.put("messageId", messageId);
		map.put("messageContent", messageContent);
		map.put("messageCollect", messageCollect);
		map.put("messageForward", messageForward);
		map.put("messageReview", messageReview);
		map.put("messageDate", date);
		map.put("messageIsForward", messageIsForward);
		map.put("messageForwardId", messageForwardId);
		map.put("messageLink", messageLink);
		map.put("messageRemark", messageRemark);
		return map;
	}
	
}