package org.scau.weibo.services;

import java.util.Date;
import java.util.List;

import org.scau.weibo.vo.BUser;

public interface IAttentionService {

	/**
	 * 根据userId查找关注人
	 * @param userId 用户编号
	 * @return  关注人列表
	 */
	public List<BUser> findAttention(long userId);

	/**
	 * 根据用户编号，为该用户添加关注人
	 * @param userId 用户编号
	 * @param attentionUserId 关注人编号
	 * @param date
	 */
	public void addAttention(long userId,long attentionUserId,Date date);

	/**
	 * 根据用户编号和关注人编号，取消用户对该关注人的关注
	 * @param userId 用户编号
	 * @param attentionUserId 关注人编号
	 */
	public void deleteAttention(long userId,long attentionUserId);

	/**
	 * 根据用户编号返回粉丝列表
	 * @param attentionUserId 用户编号
	 * @return 粉丝列表list
	 */
	public List<BUser> findFans(long attentionUserId);

	/**
	 * 根据用户id，使该用户关注粉丝
	 * @param userId 粉丝编号
	 * @param attentionUserId 用户编号
	 */
	public void addFans(long userId,long attentionUserId);

	/**
	 * 根据用户昵称，找人
	 * @param userName 用户昵称
	 * @return 用户对象集合
	 */
	public List<BUser> findByName(String userName);

	/**
	 * 根据学校名称，找人
	 * @param school 学校名
	 * @return 用户列表list
	 */
	public List<BUser> findBySchool(String school);

	/**
	 * 根据公司名称，找人
	 * @param company 公司名
	 * @return 用户列表list
	 */
	public List<BUser> findByCompany(String company);
}
