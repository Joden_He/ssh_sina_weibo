package org.scau.weibo.services;

import org.scau.weibo.vo.BUser;

public interface IUserService {

	/**
	 * 保存用户信息
	 * @param user 用户对象
	 */
	public void saveUser(BUser user);

	/**
	 * 用户注册時，保存用户邮箱是否已经存在
	 * @param userEmail 邮箱
	 * @return true/false
	 */
	public boolean vaildateUserEmail(String userEmail);

	/**
	 * 用户注册時，保存用户昵称是否已经存在
	 * @param userName 用户名称
	 * @return true/false
	 */
	public boolean vaildateUserName(String userName);

	/**
	 * 用户登录时，验证用户的信息
	 * @param userEmail 邮箱
	 * @param password 密码
	 * @return 用户对象
	 */
	public BUser vaildateUser(String userEmail,String password);

	/**
	 * 修改用户信息
	 * @param user 用户对象
	 */
	public boolean updateUser(BUser user);
}
