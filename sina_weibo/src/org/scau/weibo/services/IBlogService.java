package org.scau.weibo.services;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.scau.weibo.vo.BComment;
import org.scau.weibo.vo.BMessage;
import org.scau.weibo.vo.BUser;

public interface IBlogService {

	/**
	 * 查询微博信息
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectBlogAll();
	
	/**
	 * 根据用户id查询关注的微博信息
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectAttentionBlogAll(long userId);

	/**
	 * 查询原创微博信息
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectOriginalBlogAll();

	/**
	 * 查询@我的微博的微博信息
	 * @param userName 用户名称
	 * @param start 开始位置
	 * @param end 结束位置
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectRelateBlogAll(String userName,int start,int end);

	/**
	 * 查询原创微博信息
	 * @param start 开始位置
	 * @param end 结束位置
	 * @return 返回微博列表
	 */
	public  List<BMessage>   selectOriginalBlogAll(int start,int end);

	/**
	 * 查询有图片的微博信息
	 * @param start 开始位置
	 * @param end 结束位置
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectPictureBlogAll(int start,int end);

	/**
	 * 通过用户名查询@用户有关的微博
	 * @param userName  用户名
	 * @param page
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectBlogAll(String userName,int page);

	/**
	 * 查询微博信息
	 * @param start 开始位置
	 * @param end 结束位置
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectBlogAll(int start,int end);

	/**
	 * 根据用户Id查询微博信息
	 * @param userId 用户Id
	 * @return 返回微博列表
	 */
	public  List<BMessage> selectBlogAll(long userId);

	/**
	 * 查询微博信息
	 * @param blogId 微博ID
	 * @return 返回微博对象
	 */
	public  BMessage selectBlogl(long blogId);

	/**
	 * 增加微博信息
	 * @param blog 微博对象
	 */
	public  void addBlog(BMessage blog);

	/**
	 * 删除微博
	 * @param blogId 微博Id
	 */
	public  void  deleteBlog(long blogId);

	/**
	 * 修改收藏次数
	 * @param blogId 微博Id
	 * @param count 次数
	 */
	public  void addCollectCount(long blogId,int count);

	/**
	 * 修改转发次数
	 * @param blogId 微博Id
	 * @param count  次数
	 */
	public  void addForwardCount(long blogId,int count) ;

	/**
	 * 修改评论次数
	 * @param blogId 微博Id
	 * @param count 次数
	 */
	public  void addReviewCount(long blogId,int count);

	/**
	 * 插入收藏信息
	 * @param userId 用户Id
	 * @param blogId 微博Id
	 * @param date 收藏时间
	 */
	public  void insertCollect(long userId,long blogId,Date date);

	/**
	 * 删除收藏信息
	 * @param userId 用户Id
	 * @param blogId 微博Id
	 */
	public  void deleteCollect(long userId,long blogId);

	/**
	 * 删除收藏信息
	 * @param blogId 微博Id
	 */
	public  void deleteCollect(long blogId);

	/**
	 * 根据用户Id查询用户收藏微博信息
	 * @param userId 用户Id
	 * @return 微博对象list集合
	 */
	public List<BMessage>  selectCollect(long userId);

	/**
	 * 根据用户Id和微博Id查询用户收藏微博信息
	 * @param userId 用户Id
	 * @param blogId 微博Id
	 * @return 微博对象
	 */
	public  BMessage  queryCollect(long userId,long blogId) ;

	/**
	 * 根据微博Id查询评论信息
	 * @param blogId 微博Id
	 * @return 评论集合
	 */
	public   List<BComment>  queryComment(long blogId);

	/**
	 * 插入一条评论信息
	 * @param comment 评论对象
	 */
	public   void  insertComment(BComment  comment);

	/**
	 * 返回微博的条数
	 * @return 微博的条数
	 */
	public int getBlogSize();

	/**
	 * 根据用户Id查询用户发表的微博的条数
	 * @param userId 用户Id
	 * @return 用户发表微博的条数
	 */
	public int getBlogSize(long userId);

	/**
	 * 根据用户Id查询用户收藏的微博的条数
	 * @param userId 用户Id
	 * @param start 开始下标
	 * @param end 结束下标
	 * @return 用户发表微博的条数
	 */
	public List<BMessage> selectCollect(long userId, int start, int end) ;

	/**
	 * 根据用户Id查用户收藏的微博的条数
	 * @param userId 用户Id
	 * @return 用户收藏微博的条数
	 */
	public int getCollectBlogSize(long userId);

	/**
	 * 根据评论Id删除评论信息
	 * @param commentId 评论Id
	 */
	public void deleteComment(long commentId);

	/**
	 * 返回原创微博的条数
	 * @return 微博的条数
	 */
	public int getBlogOriginalSize();

	/**
	 * 返回有图片微博的条数
	 * @return 微博的条数
	 */
	public int getBlogPictureSize();

	/**
	 * 获取@我的微博的条数
	 * @param userName 用户名称
	 * @return @我的微博的条数
	 */
	public int getRelateBlogSize(String userName);

	/**
	 * 返回有图片微博
	 * @return 微博对象
	 */
	public List<BMessage> selectPictureBlogAll();
	
	/**
	 *  转发微博
	 * @param user
	 * @param blogId
	 */
	public void forward(BUser user,long blogId);

	/**
	 * 传递session
	 * @return
	 */
	public Session getCurrentSession();
	
	public List<BMessage> search(String k);
}
