package org.scau.weibo.services.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.scau.weibo.dao.IUserDAO;
import org.scau.weibo.services.IUserService;
import org.scau.weibo.vo.BUser;

public class UserService implements IUserService {
	//属性userDao，service层调用DAO层完成相关操作
	private IUserDAO userDAO;

	@Override
	public void saveUser(BUser user) {
		
		Date date = new Date();//获得系统时间.
		String nowTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(date);//将时间格式转换成符合Timestamp要求的格式.
		System.out.println(user);
		user.setUserRegisterDate(Timestamp.valueOf(nowTime));
		this.userDAO.saveUser(user);
	}

	@Override
	public boolean vaildateUserEmail(String userEmail) {
		
		return userDAO.validateUserEmail(userEmail);
	}

	@Override
	public boolean vaildateUserName(String userName) {
		
		return userDAO.validateUserName(userName);
	}

	@Override
	public BUser vaildateUser(String userEmail, String password) {
		
		return userDAO.validateUser(userEmail, password);
	}

	public IUserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public boolean updateUser(BUser user) {
		this.userDAO.updateUser(user);
		return true;
	}
}
