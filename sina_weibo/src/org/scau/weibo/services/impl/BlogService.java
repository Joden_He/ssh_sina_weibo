package org.scau.weibo.services.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.scau.weibo.dao.IBlogDAO;
import org.scau.weibo.services.IBlogService;
import org.scau.weibo.vo.BComment;
import org.scau.weibo.vo.BMessage;
import org.scau.weibo.vo.BUser;

public class BlogService implements IBlogService {
	
	private IBlogDAO blogDAO;

	@Override
	public List<BMessage> selectBlogAll() {
		
		return this.blogDAO.queryBlogAll();
	}

	@Override
	public List<BMessage> selectAttentionBlogAll(long userId) {
		
		return this.blogDAO.queryAttentionBlogAll(userId);
	}

	@Override
	public List<BMessage> selectOriginalBlogAll() {
		
		return this.blogDAO.queryOriginalBlogAll();
	}

	@Override
	public List<BMessage> selectRelateBlogAll(String userName, int start,
			int end) {
		
		return this.blogDAO.queryRelateBlogAll(userName, start, end);
	}

	@Override
	public List<BMessage> selectOriginalBlogAll(int start, int end) {
		
		return this.blogDAO.queryOriginalBlogAll(start, end);
	}

	@Override
	public List<BMessage> selectPictureBlogAll(int start, int end) {
		
		return this.blogDAO.queryPictureBlogAll(start, end);
	}

	@Override
	public List<BMessage> selectBlogAll(String userName, int page) {
		
		return this.blogDAO.queryBlogAll(userName, page);
	}

	@Override
	public List<BMessage> selectBlogAll(int start, int end) {
		
		return this.blogDAO.queryBlogAll(start, end);
	}

	@Override
	public List<BMessage> selectBlogAll(long userId) {
		
		return this.blogDAO.queryBlogAll(userId);
	}

	@Override
	public BMessage selectBlogl(long blogId) {
		
		return this.blogDAO.queryBlogl(blogId);
	}

	@Override
	public void addBlog(BMessage blog) {
		
		this.blogDAO.addBlog(blog);
		
	}

	@Override
	public void deleteBlog(long blogId) {
		
		this.blogDAO.deleteBlog(blogId);
	}

	@Override
	public void addCollectCount(long blogId, int count) {
		
		this.blogDAO.addCollectCount(blogId, count);
	}

	@Override
	public void addForwardCount(long blogId, int count) {
		
		this.blogDAO.addForwardCount(blogId, count);
	}
	

	@Override
	public void addReviewCount(long blogId, int count) {
		
		this.blogDAO.addReviewCount(blogId, count);
	}

	@Override
	public void insertCollect(long userId, long blogId, Date date) {
		
		this.blogDAO.insertCollect(userId, blogId, date);
	}

	@Override
	public void deleteCollect(long userId, long blogId) {
		
		this.blogDAO.deleteCollect(userId, blogId);
		
	}

	@Override
	public void deleteCollect(long blogId) {
		
		this.blogDAO.deleteCollect(blogId);
	}

	@Override
	public List<BMessage> selectCollect(long userId) {
		
		return this.blogDAO.queryCollect(userId);
	}

	@Override
	public BMessage queryCollect(long userId, long blogId) {
		
		return this.blogDAO.queryCollect(userId, blogId);
	}

	@Override
	public List<BComment> queryComment(long blogId) {
		
		return this.blogDAO.queryComment(blogId);
	}

	@Override
	public void insertComment(BComment comment) {
		
		this.blogDAO.insertComment(comment);
	}

	@Override
	public int getBlogSize() {
		return this.blogDAO.getBlogSize();
	}

	@Override
	public int getBlogSize(long userId) {
		return this.blogDAO.getBlogSize(userId);
	}

	@Override
	public List<BMessage> selectCollect(long userId, int start, int end) {
		return this.blogDAO.queryCollect(userId, start, end);
	}

	@Override
	public int getCollectBlogSize(long userId) {
		return this.blogDAO.getCollectBlogSize(userId);
	}

	@Override
	public void deleteComment(long commentId) {
		this.blogDAO.deleteComment(commentId);
	}

	@Override
	public int getBlogOriginalSize() {
		return this.blogDAO.getBlogOriginalSize();
	}

	@Override
	public int getBlogPictureSize() {
		return this.getBlogPictureSize();
	}

	@Override
	public int getRelateBlogSize(String userName) {
		return this.blogDAO.getRelateBlogSize(userName);
	}

	@Override
	public List<BMessage> selectPictureBlogAll() {
		return this.blogDAO.getPictureBlogAll();
	}
	
	@Override
	public Session getCurrentSession(){
		return this.blogDAO.getCurrentSession();
	}

	public IBlogDAO getBlogDAO() {
		return blogDAO;
	}

	public void setBlogDAO(IBlogDAO blogDAO) {
		this.blogDAO = blogDAO;
	}

	@Override
	public void forward(BUser user,long blogId) {
		this.blogDAO.forward(user, blogId);
		
	}

	@Override
	public List<BMessage> search(String k) {
		return this.blogDAO.search(k);
	}


}
