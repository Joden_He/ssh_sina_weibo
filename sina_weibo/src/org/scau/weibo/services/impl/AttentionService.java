package org.scau.weibo.services.impl;

import java.util.Date;
import java.util.List;

import org.scau.weibo.dao.impl.AttentionDAO;
import org.scau.weibo.services.IAttentionService;
import org.scau.weibo.vo.BUser;

public class AttentionService implements IAttentionService {

	private AttentionDAO attentionDAO;

	@Override
	public List<BUser> findAttention(long userId) {
		
		return this.attentionDAO.findAttention(userId);
	}

	@Override
	public void addAttention(long userId, long attentionUserId, Date date) {
		
		this.attentionDAO.addAttention(userId, attentionUserId, date);		
	}

	@Override
	public void deleteAttention(long userId, long attentionUserId) {
		
		this.attentionDAO.deleteAttention(userId, attentionUserId);		
	}

	@Override
	public List<BUser> findFans(long attentionUserId) {
		
		return this.attentionDAO.findFans(attentionUserId);
	}

	@Override
	public void addFans(long userId, long attentionUserId) {
		
		this.attentionDAO.addFans(userId, attentionUserId);		
	}

	@Override
	public List<BUser> findByName(String userName) {
		
		return this.attentionDAO.findByName(userName);
	}

	@Override
	public List<BUser> findBySchool(String school) {
		
		return this.attentionDAO.findBySchool(school);
	}

	@Override
	public List<BUser> findByCompany(String company) {
		
		return this.attentionDAO.findByCompany(company);
	}

	public AttentionDAO getAttentionDAO() {
		return attentionDAO;
	}

	public void setAttentionDAO(AttentionDAO attentionDAO) {
		this.attentionDAO = attentionDAO;
	}
}
