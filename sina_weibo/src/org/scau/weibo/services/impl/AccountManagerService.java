package org.scau.weibo.services.impl;

import java.util.List;

import org.scau.weibo.dao.impl.AccountManagerDAO;
import org.scau.weibo.services.IAccountManagerService;
import org.scau.weibo.vo.BUser;

public class AccountManagerService implements IAccountManagerService {
	
	private AccountManagerDAO accountManagerDAO;

	@Override
	public BUser getAccountAll(long userId) {
		
		return this.accountManagerDAO.getAccountAll(userId);
	}

	@Override
	public boolean checkUserName(String userName, String oldname) {
		
		return this.accountManagerDAO.checkUserName(userName);
	}

	@Override
	public boolean setAccountInfo(long userId, BUser user) {
		
		return this.accountManagerDAO.setAccountInfo(userId, user);
	}

	@Override
	public boolean setAccountEdu(long userId, String school) {
		
		return this.accountManagerDAO.setAccountEdu(userId, school);
	}

	@Override
	public List<BUser> getAccountByEdu(String school) {
		
		return this.accountManagerDAO.getAccountByEdu(school);
	}

	@Override
	public boolean setAccountProfession(long userId, String company) {
		
		return this.accountManagerDAO.setAccountProfession(userId, company);
	}

	@Override
	public boolean setAccountHeadpicture(long userId, String headurl) {
		
		return this.accountManagerDAO.setAccountHeadpicture(userId, headurl);
	}

	@Override
	public boolean modifyAccountPassword(long userId, String oldpwd,
			String newpwd) {
		
		return this.accountManagerDAO.modifyAccountPassword(userId, oldpwd, newpwd);
	}

	@Override
	public boolean resetAccountPassword(long userId, String phonenum,
			String email, String newpwd) {
		
		return this.accountManagerDAO.resetAccountPassword(userId, phonenum, newpwd);
	}

	public AccountManagerDAO getAccountManagerDAO() {
		return accountManagerDAO;
	}

	public void setAccountManagerDAO(AccountManagerDAO accountManagerDAO) {
		this.accountManagerDAO = accountManagerDAO;
	}
	
}
