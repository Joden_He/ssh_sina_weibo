package com.scau.vo;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Result entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "result", catalog = "db_dachuang")
public class Result implements java.io.Serializable {

	// Fields

	private Long id;
	private TransInfo transInfo;
	private Model model;
	private Long surviralRate;
	private Timestamp time;
	private String remark;

	// Constructors

	/** default constructor */
	public Result() {
	}

	/** minimal constructor */
	public Result(TransInfo transInfo, Model model) {
		this.transInfo = transInfo;
		this.model = model;
	}

	/** full constructor */
	public Result(TransInfo transInfo, Model model, Long surviralRate,
			Timestamp time, String remark) {
		this.transInfo = transInfo;
		this.model = model;
		this.surviralRate = surviralRate;
		this.time = time;
		this.remark = remark;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transtId", nullable = false)
	public TransInfo getTransInfo() {
		return this.transInfo;
	}

	public void setTransInfo(TransInfo transInfo) {
		this.transInfo = transInfo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelId", nullable = false)
	public Model getModel() {
		return this.model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	@Column(name = "surviralRate", precision = 10, scale = 0)
	public Long getSurviralRate() {
		return this.surviralRate;
	}

	public void setSurviralRate(Long surviralRate) {
		this.surviralRate = surviralRate;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}