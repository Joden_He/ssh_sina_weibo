package com.scau.vo;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Model entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "model", catalog = "db_dachuang")
public class Model implements java.io.Serializable {

	// Fields

	private Long id;
	private String modelName;
	private Long tempWeight;
	private Long turbWeight;
	private Long levelWeight;
	private Long oxygenWeight;
	private Integer warningValue;
	private Timestamp createTime;
	private Short isDelete;
	private String remark;
	private String formual;
	private Set<Result> results = new HashSet<Result>(0);

	// Constructors

	/** default constructor */
	public Model() {
	}

	/** minimal constructor */
	public Model(String modelName, Integer warningValue, Short isDelete) {
		this.modelName = modelName;
		this.warningValue = warningValue;
		this.isDelete = isDelete;
	}

	/** full constructor */
	public Model(String modelName, Long tempWeight, Long turbWeight,
			Long levelWeight, Long oxygenWeight, Integer warningValue,
			Timestamp createTime, Short isDelete, String remark,
			String formual, Set<Result> results) {
		this.modelName = modelName;
		this.tempWeight = tempWeight;
		this.turbWeight = turbWeight;
		this.levelWeight = levelWeight;
		this.oxygenWeight = oxygenWeight;
		this.warningValue = warningValue;
		this.createTime = createTime;
		this.isDelete = isDelete;
		this.remark = remark;
		this.formual = formual;
		this.results = results;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "modelName", nullable = false)
	public String getModelName() {
		return this.modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	@Column(name = "tempWeight", precision = 10, scale = 0)
	public Long getTempWeight() {
		return this.tempWeight;
	}

	public void setTempWeight(Long tempWeight) {
		this.tempWeight = tempWeight;
	}

	@Column(name = "turbWeight", precision = 10, scale = 0)
	public Long getTurbWeight() {
		return this.turbWeight;
	}

	public void setTurbWeight(Long turbWeight) {
		this.turbWeight = turbWeight;
	}

	@Column(name = "levelWeight", precision = 10, scale = 0)
	public Long getLevelWeight() {
		return this.levelWeight;
	}

	public void setLevelWeight(Long levelWeight) {
		this.levelWeight = levelWeight;
	}

	@Column(name = "oxygenWeight", precision = 10, scale = 0)
	public Long getOxygenWeight() {
		return this.oxygenWeight;
	}

	public void setOxygenWeight(Long oxygenWeight) {
		this.oxygenWeight = oxygenWeight;
	}

	@Column(name = "warningValue", nullable = false)
	public Integer getWarningValue() {
		return this.warningValue;
	}

	public void setWarningValue(Integer warningValue) {
		this.warningValue = warningValue;
	}

	@Column(name = "createTime", length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "isDelete", nullable = false)
	public Short getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(Short isDelete) {
		this.isDelete = isDelete;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "formual")
	public String getFormual() {
		return this.formual;
	}

	public void setFormual(String formual) {
		this.formual = formual;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "model")
	public Set<Result> getResults() {
		return this.results;
	}

	public void setResults(Set<Result> results) {
		this.results = results;
	}

}