package com.scau.vo;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * TransInfo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "trans_info", catalog = "db_dachuang")
public class TransInfo implements java.io.Serializable {

	// Fields

	private Long id;
	private String startLocation;
	private String finalLocation;
	private Timestamp createTime;
	private String remark;
	private Set<Level> levels = new HashSet<Level>(0);
	private Set<Temperature> temperatures = new HashSet<Temperature>(0);
	private Set<Oxygen> oxygens = new HashSet<Oxygen>(0);
	private Set<Result> results = new HashSet<Result>(0);
	private Set<Turbidity> turbidities = new HashSet<Turbidity>(0);

	// Constructors

	/** default constructor */
	public TransInfo() {
	}

	/** minimal constructor */
	public TransInfo(String startLocation, String finalLocation) {
		this.startLocation = startLocation;
		this.finalLocation = finalLocation;
	}

	/** full constructor */
	public TransInfo(String startLocation, String finalLocation,
			Timestamp createTime, String remark, Set<Level> levels,
			Set<Temperature> temperatures, Set<Oxygen> oxygens,
			Set<Result> results, Set<Turbidity> turbidities) {
		this.startLocation = startLocation;
		this.finalLocation = finalLocation;
		this.createTime = createTime;
		this.remark = remark;
		this.levels = levels;
		this.temperatures = temperatures;
		this.oxygens = oxygens;
		this.results = results;
		this.turbidities = turbidities;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "startLocation", nullable = false)
	public String getStartLocation() {
		return this.startLocation;
	}

	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}

	@Column(name = "finalLocation", nullable = false)
	public String getFinalLocation() {
		return this.finalLocation;
	}

	public void setFinalLocation(String finalLocation) {
		this.finalLocation = finalLocation;
	}

	@Column(name = "createTime", length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "transInfo")
	public Set<Level> getLevels() {
		return this.levels;
	}

	public void setLevels(Set<Level> levels) {
		this.levels = levels;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "transInfo")
	public Set<Temperature> getTemperatures() {
		return this.temperatures;
	}

	public void setTemperatures(Set<Temperature> temperatures) {
		this.temperatures = temperatures;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "transInfo")
	public Set<Oxygen> getOxygens() {
		return this.oxygens;
	}

	public void setOxygens(Set<Oxygen> oxygens) {
		this.oxygens = oxygens;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "transInfo")
	public Set<Result> getResults() {
		return this.results;
	}

	public void setResults(Set<Result> results) {
		this.results = results;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "transInfo")
	public Set<Turbidity> getTurbidities() {
		return this.turbidities;
	}

	public void setTurbidities(Set<Turbidity> turbidities) {
		this.turbidities = turbidities;
	}

}