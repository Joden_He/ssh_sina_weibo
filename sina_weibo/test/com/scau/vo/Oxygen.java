package com.scau.vo;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Oxygen entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "oxygen", catalog = "db_dachuang")
public class Oxygen implements java.io.Serializable {

	// Fields

	private Long id;
	private TransInfo transInfo;
	private String value;
	private Timestamp createTime;
	private String remark;

	// Constructors

	/** default constructor */
	public Oxygen() {
	}

	/** minimal constructor */
	public Oxygen(TransInfo transInfo, String value) {
		this.transInfo = transInfo;
		this.value = value;
	}

	/** full constructor */
	public Oxygen(TransInfo transInfo, String value, Timestamp createTime,
			String remark) {
		this.transInfo = transInfo;
		this.value = value;
		this.createTime = createTime;
		this.remark = remark;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transId", nullable = false)
	public TransInfo getTransInfo() {
		return this.transInfo;
	}

	public void setTransInfo(TransInfo transInfo) {
		this.transInfo = transInfo;
	}

	@Column(name = "value", nullable = false)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "createTime", length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}