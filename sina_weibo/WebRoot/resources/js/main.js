/*登录界面*/
$('input[type="text"],input[name="password"]').focus(function () {
	$(this).prev().animate({ 'opacity': '1' }, 200);
	});
$('input[type="text"],input[name="password"]').blur(function () {
	    $(this).prev().animate({ 'opacity': '.6' }, 200);
	});

/*注册界面*/
//以post的形式把用户输入的邮箱发到后台校验是否已存在
$("#userEmail").change(function(){
	if(isEmail($("#userEmail").val())){
		$.ajax({
		type : 'post',
		url : 'validateUserEmail.action',
		data : {
			"message" : $("#userEmail").val()
		},
		dataType : 'json',
		success : function(result) {
			if(result.exitUserEmail){
				$("#exitUserEmail").show();
				$("#notExitUserEmail").hide();
				$("#emailF").hide();
			}
			else{
				$("#exitUserEmail").hide();
				$("#notExitUserEmail").show();
				$("#emailF").hide();
			}
		},
		error: function(){
//			alert("系统异常，请稍后重试！");
		}
	});
	}
	else{
		$("#emailF").show();
		$("#exitUserEmail").hide();
		$("#notExitUserEmail").hide();
	}
});
//以post的形式把用户输入的用户名发到后台校验是否已存在
$("#username").change(function(){
	$.ajax({
		type : 'post',
		url :  'validateUserName.action',
		data : {
			"message" : $("#username").val()
		},
		dataType : 'json',
		success : function(result) {
			if(result.exitUserName){
				$("#exitUserName").show();
				$("#notExitUserName").hide();
			}
			else{
				$("#exitUserName").hide();
				$("#notExitUserName").show();
			}
		},
		error: function(){
//			alert("系统异常，请稍后重试！");
		}
	});
});

//前端验证注册页面输入的密码是否两次相同
$('input[name="password"],input[name="rpassword"]').blur(function () {
	  if ($("#password").val()!=$("#rpassword").val()) {
	  		if($("#rpassword").val()==""){
				$(".glyphicon-remove").css("display","none");
				$(".glyphicon-ok").css("display","block");
				$(".glyphicon-ok").css("color","");
				$(".glyphicon-ok").css("opacity",".6");
			}
			else{
				$(".glyphicon-ok").css("display","none");
				$(".glyphicon-remove").css("display","block");
			}
		}
		else{
			if($("#rpassword").val()!=""){
				$(".glyphicon-remove").css("display","none");
				$(".glyphicon-ok").css("display","block");
				$(".glyphicon-ok").css("color","#81c94c");
				$(".glyphicon-ok").css("opacity","1");
			}
			
			else{
				$(".glyphicon-remove").css("display","none");
				$(".glyphicon-ok").css("display","block");
				$(".glyphicon-ok").css("color","");
				$(".glyphicon-ok").css("opacity",".6");
			}
		}
	});

//获取后台传来的数据，判断用户密码是否正确
$("#login").click(function(){
	var username=$("#username").val();
	var password=$("#password").val();
	//以post的形式把用户输入的邮箱发到后台校验是否已存在
	$.ajax({
		type : 'post',
		url : 'login.action',
		data : {
			"user.userEmail" : username,
			"user.userPwd" : password
		},
		dataType : 'json',
		success: function(result){
			var r = eval("("+result+")");    //包数据解析为json 格式
			if(r.user==null){
				window.location.href="index.jsp";
			}
		},
		error: function(){
			alert("系统异常，请稍后重试！");
		}
	});
});

function isEmail(str){ 
	var reg =/^[\w\.-]+?@([\w\-]+\.){1,2}[a-zA-Z]{2,3}$/; 
	return true;
} 

function register(){
	$.ajax({
		type : 'post',
		url : 'register.action',
		data : {
			"user.userEmail" : $("#userEmail").val(),
			"user.userName" : $("#username").val(),
			"user.userPwd" : $("#password").val()
		},
		dataType : 'json',
		success: function(result){
			var r = eval("("+result+")");    //包数据解析为json 格式
			consloe.log(r);
		},
		error: function(){
			alert("系统异常，请稍后重试！");
		}
	});
}
