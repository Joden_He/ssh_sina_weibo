
function checkEmailOut(text){
	//以post的形式把用户输入的邮箱发到后台校验是否已存在
	$.ajax({
		type : 'post',
		url : 'validateUserEmail.action',
		data : {
			"message" : text
		},
		dataType : 'json',
		success : function(result) {
			if(result.exitUserEmail){
				$("#emailLogin").show();
				$("#emailNotice").hide();
				$("#emailError").hide();
				$("#emailTrue").hide();
			}
			else{
				$("#emailTrue").show();
				$("#emailNotice").hide();
				$("#emailError").hide();
				$("#emailLogin").hide();
			}
		}
	});
}
function checkNameOut(text){
	//以post的形式把用户输入的邮箱发到后台校验是否已存在
	$.ajax({
		type : 'post',
		url : text,
		data : {
			"message" : $("#nameId").val()
		},
		dataType : 'json',
		success : function(result) {
			if(result.exitUserName){
				$("#nameExist").show();
				$("#nameNotice").hide();
				$("#nameError").hide();
				$("#nameNull").hide();
				$("#nameTrue").hide();
			}
			else{
				$("#nameExist").hide();
				$("#nameNotice").hide();
				$("#nameError").hide();
				$("#nameNull").hide();
				$("#nameTrue").show();
			}
		}
	});
}
function isEmail(str){ 
	var reg =/^[\w\.-]+?@([\w\-]+\.){1,2}[a-zA-Z]{2,3}$/; 
	return true;
} 