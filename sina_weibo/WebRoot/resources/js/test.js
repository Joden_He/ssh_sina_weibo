$(document).ready(function(){
	list();
});

function save(){
	
	$.ajaxFileUpload(
	 		{
	 			url:'sendBlog.action?blog.messageContent='+$("#blogmessage").val(),//处理请求的Action对应的url
	 			secureuri:false,//安全传输，一般选false
	 			fileElementId:'image',//上传文件的id
	 			data : {
	 				"blog.messageContent" : $("#blogmessage").val()
	 			},
	 			dataType:'json',//服务器返回的数据类型：一般选json
	 			success : function(result) {
	 				var r = eval("("+result+")");    //包数据解析为json 格式
	 				if(r.success==true){
	 					list();
	 				}
	 				else{
	 					alert("error");
	 				}
	 			},
	 			error: function(){
//	 				alert("系统异常，请稍后重试！");
	 			}
	 		 });
	$("#blogmessage").val("");

	
	/*$.ajax({
		type : 'post',
		url : 'sendBlog.action',
		data : {
			"blog.messageContent" : $("#blogmessage").val()
		},
		dataType : 'json',
		success : function(result) {
			var r = eval("("+result+")");    //包数据解析为json 格式
			if(r.success){
				list();
			}
			else{
				alert("error");
			}
		},
		error: function(){
			alert("系统异常，请稍后重试！");
		}
	});*/
}

function list(){
	console.log("list");
	$.ajax({
		type : 'post',
		url : 'listBlogs.action',
		dataType : 'json',
		success : function(result) {
			var r = eval("("+result+")");    //包数据解析为json 格式
			console.log(r);
			var str="";
			for(var i=0;i<r.length;i++){
				str+='<div class="content-main-right-weibo div-style">'+
				'<div class="content-main-right-weibo-main">'+
					'<img src="resources/img/user.jpg" width="58" class="float-left" />'+
					'<div class="weibo-detail float-left">'+
						'<p class="weibo-user"><a href="#">'+r[i].userName+'</a></p>'+
						'<p class="weibo-time">'+r[i].messageDate+'</p>'+
						'<p class="weibo-content">'+r[i].messageContent+'</p>';
				if(r[i].messageLink!=null){
					str+='<div class="weibo-img">'+
					'<img src="images/'+r[i].messageLink+'" width="180"/>'+
					'</div>'+'</div>'+
					'<!--清除浮动-->'+
					'<div class="clearfix"></div>'+
				'</div>'+
				'<div class="content-main-right-weibo-bottom">'+
					'<ul>'+
						'<li><a href="#"><i class="iconfont icon-mima"></i><em>收藏</em></a></li>'+
						'<li><a href="forwardBlog.action?blog.messageId='+r[i].messageId +'"><i class="iconfont icon-zhuanfa" style="top:-1px; left: 46px;"></i><em>'+(r[i].messageForward==null?0:(r[i].messageForward))+'</em></a></li>'+
						'<li><a href="#"><i class="iconfont icon-pinglun" style="top:1px; left: 46px;"></i><em>'+(r[i].messageReview==null?0:r[i].messageReview)+'</em></a></li>'+
						'<li class="no-border-right"><a href="#"><i class="iconfont icon-zan"style="top:1px; left: 46px;"></i><em>53636</em></a></li>'+
						'<div class="clearfix"></div>'+
					'</ul>'+
				'</div>'+
			'</div>';
				}
				else{
					str+='</div>'+
					'<!--清除浮动-->'+
					'<div class="clearfix"></div>'+
				'</div>'+
				'<div class="content-main-right-weibo-bottom">'+
					'<ul>'+
						'<li><a href="#"><i class="iconfont icon-mima"></i><em>收藏</em></a></li>'+
						'<li><a href="forwardBlog.action?blog.messageId='+r[i].messageId +'"><i class="iconfont icon-zhuanfa" style="top:-1px; left: 46px;"></i><em>'+(r[i].messageForward==null?0:(r[i].messageForward))+'</em></a></li>'+
						'<li><a href="#"><i class="iconfont icon-pinglun" style="top:1px; left: 46px;"></i><em>'+(r[i].messageReview==null?0:r[i].messageReview)+'</em></a></li>'+
						'<li class="no-border-right"><a href="#"><i class="iconfont icon-zan"style="top:1px; left: 46px;"></i><em>53636</em></a></li>'+
						'<div class="clearfix"></div>'+
					'</ul>'+
				'</div>'+
			'</div>';
				}
			}
			if(r.length>0){
				$("#t").html(str);
			}
			
		},
		error: function(){
//			alert("系统异常，请稍后重试！");
		}
	});
}

function search1(){
	console.log("list");
	$.ajax({
		type : 'post',
		url : 'search.action',
		dataType : 'json',
		data: {
			'k': $("#search").val()
		},
		success : function(result) {
			var r = eval("("+result+")");    //包数据解析为json 格式
			console.log(r);
			var str="";
			for(var i=0;i<r.length;i++){
				str+='<div class="content-main-right-weibo div-style">'+
				'<div class="content-main-right-weibo-main">'+
					'<img src="resources/img/user.jpg" width="58" class="float-left" />'+
					'<div class="weibo-detail float-left">'+
						'<p class="weibo-user"><a href="#">'+r[i].userName+'</a></p>'+
						'<p class="weibo-time">'+r[i].messageDate+'</p>'+
						'<p class="weibo-content">'+r[i].messageContent+'</p>';
				if(r[i].messageLink!=null){
					str+='<div class="weibo-img">'+
					'<img src="images/'+r[i].messageLink+'" width="180"/>'+
					'</div>'+'</div>'+
					'<!--清除浮动-->'+
					'<div class="clearfix"></div>'+
				'</div>'+
				'<div class="content-main-right-weibo-bottom">'+
					'<ul>'+
						'<li><a href="#"><i class="iconfont icon-mima"></i><em>收藏</em></a></li>'+
						'<li><a href="forwardBlog.action?blog.messageId='+r[i].messageId +'"><i class="iconfont icon-zhuanfa" style="top:-1px; left: 46px;"></i><em>'+(r[i].messageForward==null?0:(r[i].messageForward))+'</em></a></li>'+
						'<li><a href="#"><i class="iconfont icon-pinglun" style="top:1px; left: 46px;"></i><em>'+(r[i].messageReview==null?0:r[i].messageReview)+'</em></a></li>'+
						'<li class="no-border-right"><a href="#"><i class="iconfont icon-zan"style="top:1px; left: 46px;"></i><em>53636</em></a></li>'+
						'<div class="clearfix"></div>'+
					'</ul>'+
				'</div>'+
			'</div>';
				}
				else{
					str+='</div>'+
					'<!--清除浮动-->'+
					'<div class="clearfix"></div>'+
				'</div>'+
				'<div class="content-main-right-weibo-bottom">'+
					'<ul>'+
						'<li><a href="#"><i class="iconfont icon-mima"></i><em>收藏</em></a></li>'+
						'<li><a href="forwardBlog.action?blog.messageId='+r[i].messageId +'"><i class="iconfont icon-zhuanfa" style="top:-1px; left: 46px;"></i><em>'+(r[i].messageForward==null?0:(r[i].messageForward))+'</em></a></li>'+
						'<li><a href="#"><i class="iconfont icon-pinglun" style="top:1px; left: 46px;"></i><em>'+(r[i].messageReview==null?0:r[i].messageReview)+'</em></a></li>'+
						'<li class="no-border-right"><a href="#"><i class="iconfont icon-zan"style="top:1px; left: 46px;"></i><em>53636</em></a></li>'+
						'<div class="clearfix"></div>'+
					'</ul>'+
				'</div>'+
			'</div>';
				}
			}
			if(r.length>0){
				$("#t").html(str);
			}
			
		},
		error: function(){
//			alert("系统异常，请稍后重试！");
		}
	});
}