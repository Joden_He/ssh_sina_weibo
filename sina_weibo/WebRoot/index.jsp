<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	
	<head>
		<title>微博-随时随地发生新鲜事</title>
		<link rel="shortcut icon" type="image/x-icon" href="resources/iconfont/favicon.ico" />
		<link rel="stylesheet" href="resources/css/reset.css" type="text/css">
		<link rel="stylesheet" href="resources/iconfont/iconfont.css" type="text/css">
		<link rel="stylesheet" href="resources/css/index.css" type="text/css">
		
		<script type="text/javascript" src="resources/js/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="resources/js/ajaxfileupload.js"></script>
		<meta charset="utf-8"/>
	</head>
	
	<body>
		<header>
			<!--logo-->
			<div class="header-logo float-left"></div>
			<!--搜索框-->
			<div class="header-search float-left">
				<!--搜索表单-->
				<form action="" method="post">
					<input type="input" class="search-key" name="search-key" id="search" onkeydown="search1();" placeholder="大家正在搜：元旦快乐"/>
					<span class="iconfont icon-sousuo search-submit"></span>
				</form>
			</div>
			<!--导航条-->		
			<nav class="float-right">
				<ul>
					<li><a href="index.jsp"><i class="iconfont icon-home"></i>首页</a></li>
					<li><a href="#"><i class="iconfont icon-shipin"></i>视频</a></li>
					<li><a href="#"><i class="iconfont icon-faxian" style="top: 0px;"></i>发现</a></li>
					<li><a href="#"><i class="iconfont icon-gamebar"style="top: 2px;"></i>游戏</a></li>
					<s:if test="#session.user==null">
					<li><a href="login.jsp"><i  style="top: 0px;"></i>登录</a></li>
					<li><a href="register.jsp"><i  style="top: 0px;"></i>注册</a></li>
					</s:if>
					<s:else>
					<li><a href="#"><i class="iconfont icon-person" style="top: 0px;"></i><s:property value="#session.user.userName"/></a></li>
					<li><a href="logout.action"><i  style="top: 0px;"></i>注销</a></li>
					</s:else>
				</ul>
			</nav>
			<!--清除浮动-->
			<div class="clearfix"></div>
		</header>
		
		<!--内容部分-->
		<section class="content">
			<!--内容区个人主页背景部分-->
			<s:if test="#session.user!=null">
				<div class="content-header">
					<div class="user-img">
						<img src="resources/img/user.jpg"/>
					</div>
					<p style="font-size: 22px;"><s:property value="#session.user.userName"/><p>
					<s:if test="#session.user.userIntrodute==null">
						<p style="font-size: 12px;">一句话介绍一下自己吧，让别人更了解你<p>
					</s:if>
					<s:else>
						<p style="font-size: 12px;"><s:property value="#session.user.userIntrodute"/><p>
					</s:else>
					</div>
			</s:if>
			
			<!--内容区导航栏部分-->
			<div class="content-nav">
				<ul>
					<li class="selected no-margin-left"><a href="#">我的主页</a><div></div></li>
					<li><a href="#">我的相册</a><div></div></li>
					<li><a href="#">管理中心</a><div></div></li>
				</ul>
			</div>
			
			<!--内容区主要部分-->
			<div class="content-main">
				<!--主要部分中左侧部分-->
				<aside class="content-main-left float-left">
					<div class="user-fans div-style no-margin-top">
						<ul>
							<li class="no-border-left"><p><a href="#" style="font-weight: bold;">105</p></a></p><p>关注</p></li>
							<li><p><a href="#" style="font-weight: bold;">24</p></a><p>粉丝</p></li>
							<li><p><a href="#" style="font-weight: bold;">2</p></a></p><p>微博</p></li>
						</ul>
					</div>
					<div class="advertisement div-style">
						<img src="resources/img/main_ad.jpg" width="320"/>
					</div>
					<div class="relation div-style">
						<!--标题-->
						<div class="title">微关系</div>
						<!--关注-->
						<div class="attention">
							<p class="title">他的关注(104)</p>
							<ul>
								<li>
									<a href="#"><img src="resources/img/a.jpg"></a>
									<p><a href="#">超级话题</a></p>
								</li>
								<li>
									<a href="#"><img src="resources/img/a.jpg"></a>
									<p><a href="#">经典话题</a></p>
								</li>
								<li>
									<a href="#"><img src="resources/img/b.jpg"></a>
									<p><a href="#">Airbnb</a></p>
								</li>
								<li>
									<a href="#"><img src="resources/img/c.jpg"></a>
									<p><a href="#">随手拍</a></p>
								</li>
								<!--清除浮动-->
								<div class="clearfix"></div>
							</ul>
						</div>
						<!--粉丝-->
						<div class="fans">
							<p class="title">他的粉丝(24)</p>
							<ul>
								<li>
									<a href="#"><img src="resources/img/f.jpg"></a>
									<p><a href="#">大叔控</a></p>
								</li>
								<li>
									<a href="#"><img src="resources/img/d.jpg"></a>
									<p><a href="#">善良大蛇</a></p>
								</li>
								<li>
									<a href="#"><img src="resources/img/e.jpg"></a>
									<p><a href="#">篮球小子</a></p>
								</li>
								<li>
									<a href="#"><img src="resources/img/f.jpg"></a>
									<p><a href="#">Colin</a></p>
								</li>
								<!--清除浮动-->
								<div class="clearfix"></div>
							</ul>
						</div>
						<p class="more"><a href="#">查看更多&nbsp;&gt;</a></p>
					</div>
					<!--赞-->
					<div class="good div-style">
						<div class="title">赞</div>
						<div class="good-content">
							<img src="resources/img/cyxtx.jpg" width="100px" class="float-left" />
							<p><a href="#">陳奕迅所長</a></p>
							<p><a href="#">這是內地今年的第一場。佛山，感動與感謝。</a></p>
							<div class="clearfix"></div>
						</div>
						<p class="more"><a href="#">查看更多&nbsp;&gt;</a></p>
					</div>
				</aside>
				
				<!--主要部分中显示微博部分-->
				<div class="content-main-right float-right">
				<s:if test="#session.user!=null">
					<div class="content-main-right-weibo no-margin-top" style="background:#fff; margin-bottom:20px;padding:10px">
						<span style="color:#8f94bb;">有什么新鲜事想告诉大家?</span>
						<form method="post">
							<textarea id="blogmessage"  class="form-control"  rows="8" style="border-style:solid;border-color:red;width:100%;" ></textarea>
							
							<span class="glyphicon glyphicon-picture" style="color:green;">
								<span style="color:#000;margin-left:4px;">图片</span>
								<input type="file" name="image" id="image" />
							</span>
							  
							
							<button type="button" class="btn " id="sendBlog" onclick="save();" style="float:right; margin-right:10px;">发布</button>
						</form>
					</div>
				</s:if>
					<div id="t">
				
					</div>
					<div class="content-main-right-weibo no-margin-top div-style" >
						<div class="content-main-right-weibo-main">
							<img src="resources/img/user.jpg" width="58" class="float-left" />
							<div class="weibo-detail float-left">
								<p class="weibo-user"><a href="#">陳奕迅所長</a></p>
								<p class="weibo-time">2017-01-01 00:00 来自 iPhone 6</p>
								<p class="weibo-content">擺渡人。天貓任務完成！yeah !</p>
								<div class="weibo-img">
									<img src="resources/img/weibo1.jpg" width="180"/>
								</div>
							</div>
							<!--清除浮动-->
							<div class="clearfix"></div>
						</div>
						<div class="content-main-right-weibo-bottom">
							<ul>
								<li><a href="#"><i class="iconfont icon-mima"></i><em>收藏</em></a></li>
								<li><a href="#"><i class="iconfont icon-zhuanfa" style="top:-1px; left: 46px;"></i><em>2776</em></a></li>
								<li><a href="#"><i class="iconfont icon-pinglun" style="top:1px; left: 46px;"></i><em>4867</em></a></li>
								<li class="no-border-right"><a href="#"><i class="iconfont icon-zan"style="top:1px; left: 46px;"></i><em>334526</em></a></li>
								<div class="clearfix"></div>
							</ul>
						</div>
					</div>
					<div class="content-main-right-weibo div-style">
						<div class="content-main-right-weibo-main">
							<img src="resources/img/user.jpg" width="58" class="float-left" />
							<div class="weibo-detail float-left">
								<p class="weibo-user"><a href="#">陳奕迅所長</a></p>
								<p class="weibo-time">2016-11-11 00:09 来自 iPhone 6</p>
								<p class="weibo-content">2017新年快樂！在这里恭祝各位在新的一年的心想事成，身体健康，学习进步，事业顺利！</p>
								<div class="weibo-img">
									<img src="resources/img/weibo2-3.jpg" width="180"/>
								</div>
							</div>
							<!--清除浮动-->
							<div class="clearfix"></div>
						</div>
						<div class="content-main-right-weibo-bottom">
							<ul>
								<li><a href="#"><i class="iconfont icon-mima"></i><em>收藏</em></a></li>
								<li><a href="#"><i class="iconfont icon-zhuanfa" style="top:-1px; left: 46px;"></i><em>4523</em></a></li>
								<li><a href="#"><i class="iconfont icon-pinglun" style="top:1px; left: 46px;"></i><em>3422</em></a></li>
								<li class="no-border-right"><a href="#"><i class="iconfont icon-zan"style="top:1px; left: 46px;"></i><em>53636</em></a></li>
								<div class="clearfix"></div>
							</ul>
							<div><input type="text" style="width:90%;margin:2px 3%;" ></div>
							<div style="max-height:200px;">
								<p></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!--页脚部分-->
		<footer>
			<div>
				<ul>
					<li class="no-margin-left">微博客服</li>
					<li>意见反馈</li>
					<li>舞弊举报</li>
					<li>开放平台</li>
					<li>微博招聘</li>
					<li>新浪网导航</li>
					<li>举报处理大厅</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="no-margin-left">京ICP证100780号</li>
					<li>互联网药品服务许可证</li>
					<li>互联网医疗保健许可证</li>
					<li>京网文[2014]2046-296号</li>
					<li> 京ICP备12002058号</li>
					<li> 增值电信业务经营许可证B2-20140447</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="no-margin-left">Copyright © 2009-2017 WEIBO 北京微梦创科网络技术有限公司</li>
					<li>京公网安备11000002000019号</li>
				</ul>
			</div>
		</footer>		
	</body>
	<script type="text/javascript" src="resources/js/test.js"></script>
</html>

