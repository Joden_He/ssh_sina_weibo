<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>新浪微博-随时随地分享身边的新鲜事儿</title>
	<link rel="shortcut icon" type="image/x-icon" href="resources/img/favicon.ico" />
	<link rel="stylesheet" href="resources/css/reset.css" type="text/css">
	<link rel="stylesheet" href="resources/iconfont/iconfont.css" type="text/css">
	<link rel="stylesheet" href="resources/css/index.css" type="text/css">
	<link rel="stylesheet" href="resources/bootstrap-3.3.5-dist/css/bootstrap.min.css" /> 
	<link rel="stylesheet" href="resources/css/login.css" />   
	<script src="resources/js/jquery-1.11.1.js"></script>
	<script src="resources/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/js/jquery-1.11.1.js"></script>

</head>
<body>
<header>
			<!--logo-->
			<div class="header-logo float-left"></div>
			<!--搜索框-->
			<div class="header-search float-left">
				<!--搜索表单-->
				<form action="" method="post">
					<input type="input" class="search-key" name="search-key" value="大家正在搜：元旦快乐"/>
					<span class="iconfont icon-sousuo search-submit"></span>
				</form>
			</div>
			<!--导航条-->		
			<nav class="float-right">
				<ul>
					<li><a href="index.jsp"><i class="iconfont icon-home"></i>首页</a></li>
					<li><a href="#"><i class="iconfont icon-shipin"></i>视频</a></li>
					<li><a href="#"><i class="iconfont icon-faxian" style="top: 0px;"></i>发现</a></li>
					<li><a href="#"><i class="iconfont icon-gamebar"style="top: 2px;"></i>游戏</a></li>
					<s:if test="#session.user==null">
					<li><a href="login.jsp"><i  style="top: 0px;"></i>登录</a></li>
					<li><a href="register.jsp"><i  style="top: 0px;"></i>注册</a></li>
					</s:if>
					<s:else>
					<li><a href="#"><i class="iconfont icon-person" style="top: 0px;"></i><s:property value="#session.user.userName"/></a></li>
					</s:else>
				</ul>
			</nav>
			<!--清除浮动-->
			<div class="clearfix"></div>
		</header>
	<div class="box">
		<div class="login-box">
			<div class="login-title text-center">
				<h1><small>Log In / 登录</small></h1>
			</div>
			<div class="login-content ">
			<div class="form">
 			<form id="loginForm" action="login.action" method="post">
				<div class="form-group">
					<div class="col-xs-12  ">
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
							<input type="text" id="username" name="user.userEmail" class="form-control" placeholder="Username / 用户名" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12  ">
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input type="password" id="password" name="user.userPwd" class="form-control" placeholder="Password / 密码" />
						</div>
					</div>
				</div>
				<div class="form-group form-actions">
					<div class="col-xs-4 col-xs-offset-4 ">
						<button type="submit" class="btn btn-sm btn-info" id="login"><span class="glyphicon glyphicon-off"></span> Log In / 登录</button>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-6 link">
						<p class="text-center remove-margin"><small>忘记密码？</small> <a href="javascript:void(0)" ><small>找回</small></a>
						</p>
					</div>
					<div class="col-xs-6 link">
						<p class="text-center remove-margin"><small>还没注册?</small> <a href="register.jsp" ><small>注册</small></a>
						</p>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>

		<!--页脚部分-->
		<footer>
			<div>
				<ul>
					<li class="no-margin-left">微博客服</li>
					<li>意见反馈</li>
					<li>舞弊举报</li>
					<li>开放平台</li>
					<li>微博招聘</li>
					<li>新浪网导航</li>
					<li>举报处理大厅</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="no-margin-left">京ICP证100780号</li>
					<li>互联网药品服务许可证</li>
					<li>互联网医疗保健许可证</li>
					<li>京网文[2014]2046-296号</li>
					<li> 京ICP备12002058号</li>
					<li> 增值电信业务经营许可证B2-20140447</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="no-margin-left">Copyright © 2009-2017 WEIBO 北京微梦创科网络技术有限公司</li>
					<li>京公网安备11000002000019号</li>
				</ul>
			</div>
		</footer>		
</body>
</html>
