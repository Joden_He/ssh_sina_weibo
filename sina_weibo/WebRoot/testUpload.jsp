<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert title here</title>
       <script type="text/javascript" src="resources/js/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="resources/js/ajaxfileupload.js"></script>
    <script type="text/javascript">
    	  $(function(){
    	 	$('#submit').click(function(){
    	 		$.ajaxFileUpload(
    	 		{
    	 			url:'upload/fileAction.action',//处理请求的Action对应的url
    	 			secureuri:false,//安全传输，一般选false
    	 			fileElementId:'image',//上传文件的id
    	 			dataType:'json',//服务器返回的数据类型：一般选json
    	 			success:function(data){//服务器返回数据成功的回调函数
   	 				var r = eval("("+data+")");		
    	 				if(r.url!=null||r.url==""){
    	 					str='<img src="images/'+r.url+'" />';
        	 				$("#tt").html(str);
    	 				}
    	 				else{
    	 					alert("上传失败！")
    	 				}
    	 				
    	 					},
    	 			error:function(result){//服务器返回数据失败的回调函数
    	 						alert("系统异常！");
    	 				  }
    	 		 })
    	 	})
    	}); 
    	</script>
    </head>

    <body>
    	<!-- 上传文件的form必须设置enctype为该类型且method为post -->
        <form action="" 
              enctype="multipart/form-data" 
              method="post">
              ajax测试框，请在上传图片前在文本框中随意输入内容：<br/>
             <input id="test"/><br/><br/>
           	    文件上传框：<br/>
             <input type="file" name="image" id="image"/>
	   		 <input type="button" id="submit" value="submit" /><br/>
	    	 <span id="filespan">用于显示服务器返回json内容</span>
        </form>
        <br/>
        <span id="tt"></span>
    </body>
</html>