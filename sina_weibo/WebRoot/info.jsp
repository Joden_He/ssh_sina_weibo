<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>个人信息 微博-随时随地发现新鲜事</title>
		<link rel="shortcut icon" type="image/x-icon" href="resources/iconfont/favicon.ico" />
		<link rel="stylesheet" href="resources/css/reset.css" type="text/css">
		<link rel="stylesheet" href="resources/iconfont/iconfont.css" type="text/css">
		<link rel="stylesheet" href="resources/css/index.css" type="text/css">
		
		<script type="text/javascript" src="resources/js/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="resources/js/ajaxfileupload.js"></script>
		<meta charset="utf-8"/>
	</head>
	
	<body>
		<header>
			<!--logo-->
			<div class="header-logo float-left"></div>
			<!--搜索框-->
			<div class="header-search float-left">
				<!--搜索表单-->
				<form action="" method="post">
					<input type="input" class="search-key" name="search-key" id="search" onkeydown="search1();" placeholder="大家正在搜：元旦快乐"/>
					<span class="iconfont icon-sousuo search-submit"></span>
				</form>
			</div>
			<!--导航条-->		
			<nav class="float-right">
				<ul>
					<li><a href="index.jsp"><i class="iconfont icon-home"></i>首页</a></li>
					<li><a href="#"><i class="iconfont icon-shipin"></i>视频</a></li>
					<li><a href="#"><i class="iconfont icon-faxian" style="top: 0px;"></i>发现</a></li>
					<li><a href="#"><i class="iconfont icon-gamebar"style="top: 2px;"></i>游戏</a></li>
					<s:if test="#session.user==null">
					<li><a href="login.jsp"><i  style="top: 0px;"></i>登录</a></li>
					<li><a href="register.jsp"><i  style="top: 0px;"></i>注册</a></li>
					</s:if>
					<s:else>
					<li><a href="#"><i class="iconfont icon-person" style="top: 0px;"></i><s:property value="#session.user.userName"/></a></li>
					<li><a href="logout.action"><i  style="top: 0px;"></i>注销</a></li>
					</s:else>
				</ul>
			</nav>
			<!--清除浮动-->
			<div class="clearfix"></div>
		</header>
		
		<!--内容部分-->
		<section class="content">
			<!--内容区个人主页背景部分-->
			<s:if test="#session.user!=null">
				<div class="content-header">
					<div class="user-img">
						<img src="resources/img/user.jpg"/>
					</div>
					<p style="font-size: 22px;"><s:property value="#session.user.userName"/><p>
					<s:if test="#session.user.userIntrodute==null">
						<p style="font-size: 12px;">一句话介绍一下自己吧，让别人更了解你<p>
					</s:if>
					<s:else>
						<p style="font-size: 12px;"><s:property value="#session.user.userIntrodute"/><p>
					</s:else>
					</div>
			</s:if>
			
			<!--内容区导航栏部分-->
			<div class="content-nav" style="height:800px;">
				<div style="margin-left:-200px;">
					<h2 >个人信息</h2>
					<form action="updateUser.action">
						昵称：<s:property value="#session.user.userName"/><br>
						个人简介：<input name="user.userIntrodute"    value="<s:property value="#session.user.userIntrodute"/>" /><br>
						性别：<input name="user.userSex"    value="<s:property value="#session.user.userSex"/>" /><br>
						毕业院校：<input name="user.userSchool"     value="<s:property value="#session.user.userSchool"/>" /><br>
						公司：<input name="user.userCompany"   value="<s:property value="#session.user.userCompany"/>" /><br>
						住址：<input name="user.userProvince"  value="<s:property value="#session.user.userProvince"/>" />省
						<input name="user.userCity"   value="<s:property value="#session.user.userCity"/>" />市<br>
					<%-- 	更改头像： <input type="file" name="image" id="image"/>" /><br><span id="userHead"></span>
						更改背景： <input type="file" name="image" id="image"/>" /><br><span id="bg"></span> --%>
						<input type="submit" id="m" value="修改">
					</form>
					<script type="text/javascript">
						$("#m").click(function(){
							
						});
						
					</script>
				</div>
			</div>
		</section>
		
		<!--页脚部分-->
		<footer>
			<div>
				<ul>
					<li class="no-margin-left">微博客服</li>
					<li>意见反馈</li>
					<li>舞弊举报</li>
					<li>开放平台</li>
					<li>微博招聘</li>
					<li>新浪网导航</li>
					<li>举报处理大厅</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="no-margin-left">京ICP证100780号</li>
					<li>互联网药品服务许可证</li>
					<li>互联网医疗保健许可证</li>
					<li>京网文[2014]2046-296号</li>
					<li> 京ICP备12002058号</li>
					<li> 增值电信业务经营许可证B2-20140447</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="no-margin-left">Copyright © 2009-2017 WEIBO 北京微梦创科网络技术有限公司</li>
					<li>京公网安备11000002000019号</li>
				</ul>
			</div>
		</footer>		
	</body>
	<script type="text/javascript" src="resources/js/test.js"></script>
</html>


