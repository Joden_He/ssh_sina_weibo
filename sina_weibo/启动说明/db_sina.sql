/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : db_sina

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2016-12-28 21:42:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for b_attention
-- ----------------------------
DROP TABLE IF EXISTS `b_attention`;
CREATE TABLE `b_attention` (
  `attentionId` bigint(30) NOT NULL AUTO_INCREMENT,
  `userId` bigint(30) NOT NULL,
  `attentionUserId` bigint(30) DEFAULT NULL,
  `attentionDate` dateTime DEFAULT NULL,
  `attentionRemark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`attentionId`),
  KEY `FK_B_ATTENT_REFERENCE_B_USER` (`userId`),
  CONSTRAINT `FK_B_ATTENT_REFERENCE_B_USER` FOREIGN KEY (`userId`) REFERENCES `b_user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of b_attention
-- ----------------------------

-- ----------------------------
-- Table structure for b_collect
-- ----------------------------
DROP TABLE IF EXISTS `b_collect`;
CREATE TABLE `b_collect` (
  `collectId` bigint(30) NOT NULL AUTO_INCREMENT,
  `userId` bigint(30) NOT NULL,
  `messageId` bigint(30) NOT NULL,
  `collectDate` dateTime DEFAULT NULL,
  `collectRemark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`collectId`),
  KEY `FK_B_COLLEC_REFERENCE_B_USER` (`userId`),
  KEY `FK_B_COLLEC_REFERENCE_B_MESSAG` (`messageId`),
  CONSTRAINT `FK_B_COLLEC_REFERENCE_B_MESSAG` FOREIGN KEY (`messageId`) REFERENCES `b_message` (`messageId`),
  CONSTRAINT `FK_B_COLLEC_REFERENCE_B_USER` FOREIGN KEY (`userId`) REFERENCES `b_user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of b_collect
-- ----------------------------

-- ----------------------------
-- Table structure for b_comment
-- ----------------------------
DROP TABLE IF EXISTS `b_comment`;
CREATE TABLE `b_comment` (
  `commentId` bigint(30) NOT NULL AUTO_INCREMENT,
  `userId` bigint(30) DEFAULT NULL,
  `messageId` bigint(30) NOT NULL,
  `messageContent` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageDate` dateTime DEFAULT NULL,
  `messageRemark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentedUserId` bigint(30) DEFAULT NULL,
  PRIMARY KEY (`commentId`),
  KEY `FK_B_COMMEN_REFERENCE_B_USER` (`userId`),
  KEY `FK_B_COMMEN_REFERENCE_B_MESSAG` (`messageId`),
  CONSTRAINT `FK_B_COMMEN_REFERENCE_B_MESSAG` FOREIGN KEY (`messageId`) REFERENCES `b_message` (`messageId`),
  CONSTRAINT `FK_B_COMMEN_REFERENCE_B_USER` FOREIGN KEY (`userId`) REFERENCES `b_user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of b_comment
-- ----------------------------

-- ----------------------------
-- Table structure for b_message
-- ----------------------------
DROP TABLE IF EXISTS `b_message`;
CREATE TABLE `b_message` (
  `messageId` bigint(30) NOT NULL AUTO_INCREMENT COMMENT '消息编号',
  `userId` bigint(30) NOT NULL COMMENT '用户编号',
  `messageContent` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '消息内容',
  `messageCollect` bigint(50) DEFAULT NULL COMMENT '收藏次数',
  `messageForward` bigint(20) DEFAULT NULL COMMENT '转发次数',
  `messageReview` bigint(10) DEFAULT NULL COMMENT '评论次数',
  `messageDate` dateTime DEFAULT NULL COMMENT '发表时间',
  `messageIsForward` tinyint(50) DEFAULT NULL COMMENT '是否转发，1代表是，0代表否',
  `messageForwardId` bigint(30) DEFAULT NULL COMMENT '转发消息编号',
  `messageLink` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '链接地址',
  `messageRemark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`messageId`),
  KEY `FK_B_MESSAG_REFERENCE_B_USER` (`userId`),
  CONSTRAINT `FK_B_MESSAG_REFERENCE_B_USER` FOREIGN KEY (`userId`) REFERENCES `b_user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of b_message
-- ----------------------------

-- ----------------------------
-- Table structure for b_user
-- ----------------------------
DROP TABLE IF EXISTS `b_user`;
CREATE TABLE `b_user` (
  `userId` bigint(30) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `userName` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户昵称',
  `userHead` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户头像(url)',
  `userEmail` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email',
  `userPwd` varchar(20) COLLATE utf8_unicode_ci DEFAULT '123456',
  `userSex` varchar(10) COLLATE utf8_unicode_ci DEFAULT '男' COMMENT '用户性别',
  `userSchool` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '学校',
  `userCompany` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司名',
  `userPhone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号',
  `userProvince` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '居住省',
  `userCity` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '居住城市',
  `userBlogAddress` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRegisterDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userBirthday` datetime DEFAULT NULL COMMENT '生日',
  `usrBackground` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userIntrodute` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '个人介绍',
  `userRemark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of b_user
-- ----------------------------
INSERT INTO `b_user` VALUES ('1', 'Joden', null, '824923282@qq.com', '123456', '男', null, null, null, null, null, null, '2016-12-09 23:05:01', '2016-12-09 00:00:00', null, null, null);
INSERT INTO `b_user` VALUES ('2', 'Fancy', null, '985609989@qq.com', '123456', '女', null, null, null, null, null, null, '2016-12-09 23:11:23', '1996-01-26 23:10:36', null, null, null);
INSERT INTO `b_user` VALUES ('3', 'test1', null, '8249232821@qq.com', '1234561', '女', null, null, null, null, null, null, '2016-12-28 16:42:52', null, null, null, null);
INSERT INTO `b_user` VALUES ('4', 'test2', null, '321@qq.com', '1234561', '女', null, null, null, null, null, null, '2016-12-28 16:43:25', null, null, null, null);
INSERT INTO `b_user` VALUES ('5', '何晓锋', null, '1231@qq.com', '1234561', '女', null, null, null, null, null, null, '2016-12-28 16:43:35', null, null, null, null);
INSERT INTO `b_user` VALUES ('6', 'test2', null, '123@qq.com', '123456', '男', null, null, null, null, null, null, '2016-12-28 16:43:02', null, null, null, null);
SET FOREIGN_KEY_CHECKS=1;
